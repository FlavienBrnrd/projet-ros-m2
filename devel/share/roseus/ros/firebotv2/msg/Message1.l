;; Auto-generated. Do not edit!


(when (boundp 'firebotv2::Message1)
  (if (not (find-package "FIREBOTV2"))
    (make-package "FIREBOTV2"))
  (shadow 'Message1 (find-package "FIREBOTV2")))
(unless (find-package "FIREBOTV2::MESSAGE1")
  (make-package "FIREBOTV2::MESSAGE1"))

(in-package "ROS")
;;//! \htmlinclude Message1.msg.html


(defclass firebotv2::Message1
  :super ros::object
  :slots ())

(defmethod firebotv2::Message1
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(setf (get firebotv2::Message1 :md5sum-) "d41d8cd98f00b204e9800998ecf8427e")
(setf (get firebotv2::Message1 :datatype-) "firebotv2/Message1")
(setf (get firebotv2::Message1 :definition-)
      "
")



(provide :firebotv2/Message1 "d41d8cd98f00b204e9800998ecf8427e")


