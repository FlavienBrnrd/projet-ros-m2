;; Auto-generated. Do not edit!


(when (boundp 'firebotv2::motion)
  (if (not (find-package "FIREBOTV2"))
    (make-package "FIREBOTV2"))
  (shadow 'motion (find-package "FIREBOTV2")))
(unless (find-package "FIREBOTV2::MOTION")
  (make-package "FIREBOTV2::MOTION"))

(in-package "ROS")
;;//! \htmlinclude motion.msg.html


(defclass firebotv2::motion
  :super ros::object
  :slots (_motion ))

(defmethod firebotv2::motion
  (:init
   (&key
    ((:motion __motion) 0)
    )
   (send-super :init)
   (setq _motion (round __motion))
   self)
  (:motion
   (&optional __motion)
   (if __motion (setq _motion __motion)) _motion)
  (:serialization-length
   ()
   (+
    ;; int8 _motion
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int8 _motion
       (write-byte _motion s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int8 _motion
     (setq _motion (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> _motion 127) (setq _motion (- _motion 256)))
   ;;
   self)
  )

(setf (get firebotv2::motion :md5sum-) "1809c2e9831fecb694bca727338cc93e")
(setf (get firebotv2::motion :datatype-) "firebotv2/motion")
(setf (get firebotv2::motion :definition-)
      "int8 motion

")



(provide :firebotv2/motion "1809c2e9831fecb694bca727338cc93e")


