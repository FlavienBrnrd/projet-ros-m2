;; Auto-generated. Do not edit!


(when (boundp 'firebotv2::Service1)
  (if (not (find-package "FIREBOTV2"))
    (make-package "FIREBOTV2"))
  (shadow 'Service1 (find-package "FIREBOTV2")))
(unless (find-package "FIREBOTV2::SERVICE1")
  (make-package "FIREBOTV2::SERVICE1"))
(unless (find-package "FIREBOTV2::SERVICE1REQUEST")
  (make-package "FIREBOTV2::SERVICE1REQUEST"))
(unless (find-package "FIREBOTV2::SERVICE1RESPONSE")
  (make-package "FIREBOTV2::SERVICE1RESPONSE"))

(in-package "ROS")





(defclass firebotv2::Service1Request
  :super ros::object
  :slots ())

(defmethod firebotv2::Service1Request
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(defclass firebotv2::Service1Response
  :super ros::object
  :slots ())

(defmethod firebotv2::Service1Response
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(defclass firebotv2::Service1
  :super ros::object
  :slots ())

(setf (get firebotv2::Service1 :md5sum-) "d41d8cd98f00b204e9800998ecf8427e")
(setf (get firebotv2::Service1 :datatype-) "firebotv2/Service1")
(setf (get firebotv2::Service1 :request) firebotv2::Service1Request)
(setf (get firebotv2::Service1 :response) firebotv2::Service1Response)

(defmethod firebotv2::Service1Request
  (:response () (instance firebotv2::Service1Response :init)))

(setf (get firebotv2::Service1Request :md5sum-) "d41d8cd98f00b204e9800998ecf8427e")
(setf (get firebotv2::Service1Request :datatype-) "firebotv2/Service1Request")
(setf (get firebotv2::Service1Request :definition-)
      "
---
")

(setf (get firebotv2::Service1Response :md5sum-) "d41d8cd98f00b204e9800998ecf8427e")
(setf (get firebotv2::Service1Response :datatype-) "firebotv2/Service1Response")
(setf (get firebotv2::Service1Response :definition-)
      "
---
")



(provide :firebotv2/Service1 "d41d8cd98f00b204e9800998ecf8427e")


