;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::robot_output)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'robot_output (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::ROBOT_OUTPUT")
  (make-package "ASSEMBLY_CONTROL_ROS::ROBOT_OUTPUT"))

(in-package "ROS")
;;//! \htmlinclude robot_output.msg.html


(defclass assembly_control_ros::robot_output
  :super ros::object
  :slots (_go_left_done _go_right_done _go_grasp_done _go_release_done _assemble_1_done _assemble_2_done _assemble_3_done ))

(defmethod assembly_control_ros::robot_output
  (:init
   (&key
    ((:go_left_done __go_left_done) nil)
    ((:go_right_done __go_right_done) nil)
    ((:go_grasp_done __go_grasp_done) nil)
    ((:go_release_done __go_release_done) nil)
    ((:assemble_1_done __assemble_1_done) nil)
    ((:assemble_2_done __assemble_2_done) nil)
    ((:assemble_3_done __assemble_3_done) nil)
    )
   (send-super :init)
   (setq _go_left_done __go_left_done)
   (setq _go_right_done __go_right_done)
   (setq _go_grasp_done __go_grasp_done)
   (setq _go_release_done __go_release_done)
   (setq _assemble_1_done __assemble_1_done)
   (setq _assemble_2_done __assemble_2_done)
   (setq _assemble_3_done __assemble_3_done)
   self)
  (:go_left_done
   (&optional __go_left_done)
   (if __go_left_done (setq _go_left_done __go_left_done)) _go_left_done)
  (:go_right_done
   (&optional __go_right_done)
   (if __go_right_done (setq _go_right_done __go_right_done)) _go_right_done)
  (:go_grasp_done
   (&optional __go_grasp_done)
   (if __go_grasp_done (setq _go_grasp_done __go_grasp_done)) _go_grasp_done)
  (:go_release_done
   (&optional __go_release_done)
   (if __go_release_done (setq _go_release_done __go_release_done)) _go_release_done)
  (:assemble_1_done
   (&optional __assemble_1_done)
   (if __assemble_1_done (setq _assemble_1_done __assemble_1_done)) _assemble_1_done)
  (:assemble_2_done
   (&optional __assemble_2_done)
   (if __assemble_2_done (setq _assemble_2_done __assemble_2_done)) _assemble_2_done)
  (:assemble_3_done
   (&optional __assemble_3_done)
   (if __assemble_3_done (setq _assemble_3_done __assemble_3_done)) _assemble_3_done)
  (:serialization-length
   ()
   (+
    ;; bool _go_left_done
    1
    ;; bool _go_right_done
    1
    ;; bool _go_grasp_done
    1
    ;; bool _go_release_done
    1
    ;; bool _assemble_1_done
    1
    ;; bool _assemble_2_done
    1
    ;; bool _assemble_3_done
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _go_left_done
       (if _go_left_done (write-byte -1 s) (write-byte 0 s))
     ;; bool _go_right_done
       (if _go_right_done (write-byte -1 s) (write-byte 0 s))
     ;; bool _go_grasp_done
       (if _go_grasp_done (write-byte -1 s) (write-byte 0 s))
     ;; bool _go_release_done
       (if _go_release_done (write-byte -1 s) (write-byte 0 s))
     ;; bool _assemble_1_done
       (if _assemble_1_done (write-byte -1 s) (write-byte 0 s))
     ;; bool _assemble_2_done
       (if _assemble_2_done (write-byte -1 s) (write-byte 0 s))
     ;; bool _assemble_3_done
       (if _assemble_3_done (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _go_left_done
     (setq _go_left_done (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _go_right_done
     (setq _go_right_done (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _go_grasp_done
     (setq _go_grasp_done (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _go_release_done
     (setq _go_release_done (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _assemble_1_done
     (setq _assemble_1_done (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _assemble_2_done
     (setq _assemble_2_done (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _assemble_3_done
     (setq _assemble_3_done (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::robot_output :md5sum-) "f860ddfdf175592b078c56f3a3fb1cee")
(setf (get assembly_control_ros::robot_output :datatype-) "assembly_control_ros/robot_output")
(setf (get assembly_control_ros::robot_output :definition-)
      "bool go_left_done
bool go_right_done
bool go_grasp_done
bool go_release_done
bool assemble_1_done
bool assemble_2_done
bool assemble_3_done

")



(provide :assembly_control_ros/robot_output "f860ddfdf175592b078c56f3a3fb1cee")


