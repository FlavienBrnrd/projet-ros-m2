;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::evacuation_conveyor_output)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'evacuation_conveyor_output (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::EVACUATION_CONVEYOR_OUTPUT")
  (make-package "ASSEMBLY_CONTROL_ROS::EVACUATION_CONVEYOR_OUTPUT"))

(in-package "ROS")
;;//! \htmlinclude evacuation_conveyor_output.msg.html


(defclass assembly_control_ros::evacuation_conveyor_output
  :super ros::object
  :slots (_EC_STOPPED ))

(defmethod assembly_control_ros::evacuation_conveyor_output
  (:init
   (&key
    ((:EC_STOPPED __EC_STOPPED) nil)
    )
   (send-super :init)
   (setq _EC_STOPPED __EC_STOPPED)
   self)
  (:EC_STOPPED
   (&optional __EC_STOPPED)
   (if __EC_STOPPED (setq _EC_STOPPED __EC_STOPPED)) _EC_STOPPED)
  (:serialization-length
   ()
   (+
    ;; bool _EC_STOPPED
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _EC_STOPPED
       (if _EC_STOPPED (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _EC_STOPPED
     (setq _EC_STOPPED (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::evacuation_conveyor_output :md5sum-) "c09c96dee6aef70265cad5be94ee4a4d")
(setf (get assembly_control_ros::evacuation_conveyor_output :datatype-) "assembly_control_ros/evacuation_conveyor_output")
(setf (get assembly_control_ros::evacuation_conveyor_output :definition-)
      "bool EC_STOPPED
")



(provide :assembly_control_ros/evacuation_conveyor_output "c09c96dee6aef70265cad5be94ee4a4d")


