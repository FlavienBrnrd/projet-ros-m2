;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::camera_state)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'camera_state (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::CAMERA_STATE")
  (make-package "ASSEMBLY_CONTROL_ROS::CAMERA_STATE"))

(in-package "ROS")
;;//! \htmlinclude camera_state.msg.html


(defclass assembly_control_ros::camera_state
  :super ros::object
  :slots (_done _part ))

(defmethod assembly_control_ros::camera_state
  (:init
   (&key
    ((:done __done) nil)
    ((:part __part) 0)
    )
   (send-super :init)
   (setq _done __done)
   (setq _part (round __part))
   self)
  (:done
   (&optional __done)
   (if __done (setq _done __done)) _done)
  (:part
   (&optional __part)
   (if __part (setq _part __part)) _part)
  (:serialization-length
   ()
   (+
    ;; bool _done
    1
    ;; int8 _part
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _done
       (if _done (write-byte -1 s) (write-byte 0 s))
     ;; int8 _part
       (write-byte _part s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _done
     (setq _done (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; int8 _part
     (setq _part (sys::peek buf ptr- :char)) (incf ptr- 1)
     (if (> _part 127) (setq _part (- _part 256)))
   ;;
   self)
  )

(setf (get assembly_control_ros::camera_state :md5sum-) "e285620228be6451d908510458fc988c")
(setf (get assembly_control_ros::camera_state :datatype-) "assembly_control_ros/camera_state")
(setf (get assembly_control_ros::camera_state :definition-)
      "bool done
int8 part
")



(provide :assembly_control_ros/camera_state "e285620228be6451d908510458fc988c")


