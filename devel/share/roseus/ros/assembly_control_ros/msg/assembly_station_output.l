;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::assembly_station_output)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'assembly_station_output (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::ASSEMBLY_STATION_OUTPUT")
  (make-package "ASSEMBLY_CONTROL_ROS::ASSEMBLY_STATION_OUTPUT"))

(in-package "ROS")
;;//! \htmlinclude assembly_station_output.msg.html


(defclass assembly_control_ros::assembly_station_output
  :super ros::object
  :slots (_AS_VALID ))

(defmethod assembly_control_ros::assembly_station_output
  (:init
   (&key
    ((:AS_VALID __AS_VALID) nil)
    )
   (send-super :init)
   (setq _AS_VALID __AS_VALID)
   self)
  (:AS_VALID
   (&optional __AS_VALID)
   (if __AS_VALID (setq _AS_VALID __AS_VALID)) _AS_VALID)
  (:serialization-length
   ()
   (+
    ;; bool _AS_VALID
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _AS_VALID
       (if _AS_VALID (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _AS_VALID
     (setq _AS_VALID (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::assembly_station_output :md5sum-) "dc50fef128a1d9b4a668a05575aa5293")
(setf (get assembly_control_ros::assembly_station_output :datatype-) "assembly_control_ros/assembly_station_output")
(setf (get assembly_control_ros::assembly_station_output :definition-)
      "bool AS_VALID
")



(provide :assembly_control_ros/assembly_station_output "dc50fef128a1d9b4a668a05575aa5293")


