;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::robot_input)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'robot_input (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::ROBOT_INPUT")
  (make-package "ASSEMBLY_CONTROL_ROS::ROBOT_INPUT"))

(in-package "ROS")
;;//! \htmlinclude robot_input.msg.html


(defclass assembly_control_ros::robot_input
  :super ros::object
  :slots (_go_left _go_right _go_grasp _go_release _go_assemble_1 _go_assemble_2 _go_assemble_3 ))

(defmethod assembly_control_ros::robot_input
  (:init
   (&key
    ((:go_left __go_left) nil)
    ((:go_right __go_right) nil)
    ((:go_grasp __go_grasp) nil)
    ((:go_release __go_release) nil)
    ((:go_assemble_1 __go_assemble_1) nil)
    ((:go_assemble_2 __go_assemble_2) nil)
    ((:go_assemble_3 __go_assemble_3) nil)
    )
   (send-super :init)
   (setq _go_left __go_left)
   (setq _go_right __go_right)
   (setq _go_grasp __go_grasp)
   (setq _go_release __go_release)
   (setq _go_assemble_1 __go_assemble_1)
   (setq _go_assemble_2 __go_assemble_2)
   (setq _go_assemble_3 __go_assemble_3)
   self)
  (:go_left
   (&optional __go_left)
   (if __go_left (setq _go_left __go_left)) _go_left)
  (:go_right
   (&optional __go_right)
   (if __go_right (setq _go_right __go_right)) _go_right)
  (:go_grasp
   (&optional __go_grasp)
   (if __go_grasp (setq _go_grasp __go_grasp)) _go_grasp)
  (:go_release
   (&optional __go_release)
   (if __go_release (setq _go_release __go_release)) _go_release)
  (:go_assemble_1
   (&optional __go_assemble_1)
   (if __go_assemble_1 (setq _go_assemble_1 __go_assemble_1)) _go_assemble_1)
  (:go_assemble_2
   (&optional __go_assemble_2)
   (if __go_assemble_2 (setq _go_assemble_2 __go_assemble_2)) _go_assemble_2)
  (:go_assemble_3
   (&optional __go_assemble_3)
   (if __go_assemble_3 (setq _go_assemble_3 __go_assemble_3)) _go_assemble_3)
  (:serialization-length
   ()
   (+
    ;; bool _go_left
    1
    ;; bool _go_right
    1
    ;; bool _go_grasp
    1
    ;; bool _go_release
    1
    ;; bool _go_assemble_1
    1
    ;; bool _go_assemble_2
    1
    ;; bool _go_assemble_3
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _go_left
       (if _go_left (write-byte -1 s) (write-byte 0 s))
     ;; bool _go_right
       (if _go_right (write-byte -1 s) (write-byte 0 s))
     ;; bool _go_grasp
       (if _go_grasp (write-byte -1 s) (write-byte 0 s))
     ;; bool _go_release
       (if _go_release (write-byte -1 s) (write-byte 0 s))
     ;; bool _go_assemble_1
       (if _go_assemble_1 (write-byte -1 s) (write-byte 0 s))
     ;; bool _go_assemble_2
       (if _go_assemble_2 (write-byte -1 s) (write-byte 0 s))
     ;; bool _go_assemble_3
       (if _go_assemble_3 (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _go_left
     (setq _go_left (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _go_right
     (setq _go_right (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _go_grasp
     (setq _go_grasp (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _go_release
     (setq _go_release (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _go_assemble_1
     (setq _go_assemble_1 (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _go_assemble_2
     (setq _go_assemble_2 (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _go_assemble_3
     (setq _go_assemble_3 (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::robot_input :md5sum-) "b449b320c3b2a2b17149f74a856e21ae")
(setf (get assembly_control_ros::robot_input :datatype-) "assembly_control_ros/robot_input")
(setf (get assembly_control_ros::robot_input :definition-)
      "bool go_left
bool go_right
bool go_grasp
bool go_release
bool go_assemble_1
bool go_assemble_2
bool go_assemble_3

")



(provide :assembly_control_ros/robot_input "b449b320c3b2a2b17149f74a856e21ae")


