;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::evacuation_conveyor_state)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'evacuation_conveyor_state (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::EVACUATION_CONVEYOR_STATE")
  (make-package "ASSEMBLY_CONTROL_ROS::EVACUATION_CONVEYOR_STATE"))

(in-package "ROS")
;;//! \htmlinclude evacuation_conveyor_state.msg.html


(defclass assembly_control_ros::evacuation_conveyor_state
  :super ros::object
  :slots (_stopped ))

(defmethod assembly_control_ros::evacuation_conveyor_state
  (:init
   (&key
    ((:stopped __stopped) nil)
    )
   (send-super :init)
   (setq _stopped __stopped)
   self)
  (:stopped
   (&optional __stopped)
   (if __stopped (setq _stopped __stopped)) _stopped)
  (:serialization-length
   ()
   (+
    ;; bool _stopped
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _stopped
       (if _stopped (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _stopped
     (setq _stopped (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::evacuation_conveyor_state :md5sum-) "caab21a24341159709db962b47b01ad1")
(setf (get assembly_control_ros::evacuation_conveyor_state :datatype-) "assembly_control_ros/evacuation_conveyor_state")
(setf (get assembly_control_ros::evacuation_conveyor_state :definition-)
      "bool stopped
")



(provide :assembly_control_ros/evacuation_conveyor_state "caab21a24341159709db962b47b01ad1")


