;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::supply_conveyor_state)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'supply_conveyor_state (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::SUPPLY_CONVEYOR_STATE")
  (make-package "ASSEMBLY_CONTROL_ROS::SUPPLY_CONVEYOR_STATE"))

(in-package "ROS")
;;//! \htmlinclude supply_conveyor_state.msg.html


(defclass assembly_control_ros::supply_conveyor_state
  :super ros::object
  :slots (_optical_barrier ))

(defmethod assembly_control_ros::supply_conveyor_state
  (:init
   (&key
    ((:optical_barrier __optical_barrier) nil)
    )
   (send-super :init)
   (setq _optical_barrier __optical_barrier)
   self)
  (:optical_barrier
   (&optional __optical_barrier)
   (if __optical_barrier (setq _optical_barrier __optical_barrier)) _optical_barrier)
  (:serialization-length
   ()
   (+
    ;; bool _optical_barrier
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _optical_barrier
       (if _optical_barrier (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _optical_barrier
     (setq _optical_barrier (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::supply_conveyor_state :md5sum-) "36bbab7385881955fe117c4d8550a944")
(setf (get assembly_control_ros::supply_conveyor_state :datatype-) "assembly_control_ros/supply_conveyor_state")
(setf (get assembly_control_ros::supply_conveyor_state :definition-)
      "bool optical_barrier
")



(provide :assembly_control_ros/supply_conveyor_state "36bbab7385881955fe117c4d8550a944")


