;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::camera_output)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'camera_output (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::CAMERA_OUTPUT")
  (make-package "ASSEMBLY_CONTROL_ROS::CAMERA_OUTPUT"))

(in-package "ROS")
;;//! \htmlinclude camera_output.msg.html


(defclass assembly_control_ros::camera_output
  :super ros::object
  :slots (_part_analyzed _part1 _part2 _part3 ))

(defmethod assembly_control_ros::camera_output
  (:init
   (&key
    ((:part_analyzed __part_analyzed) nil)
    ((:part1 __part1) nil)
    ((:part2 __part2) nil)
    ((:part3 __part3) nil)
    )
   (send-super :init)
   (setq _part_analyzed __part_analyzed)
   (setq _part1 __part1)
   (setq _part2 __part2)
   (setq _part3 __part3)
   self)
  (:part_analyzed
   (&optional __part_analyzed)
   (if __part_analyzed (setq _part_analyzed __part_analyzed)) _part_analyzed)
  (:part1
   (&optional __part1)
   (if __part1 (setq _part1 __part1)) _part1)
  (:part2
   (&optional __part2)
   (if __part2 (setq _part2 __part2)) _part2)
  (:part3
   (&optional __part3)
   (if __part3 (setq _part3 __part3)) _part3)
  (:serialization-length
   ()
   (+
    ;; bool _part_analyzed
    1
    ;; bool _part1
    1
    ;; bool _part2
    1
    ;; bool _part3
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _part_analyzed
       (if _part_analyzed (write-byte -1 s) (write-byte 0 s))
     ;; bool _part1
       (if _part1 (write-byte -1 s) (write-byte 0 s))
     ;; bool _part2
       (if _part2 (write-byte -1 s) (write-byte 0 s))
     ;; bool _part3
       (if _part3 (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _part_analyzed
     (setq _part_analyzed (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _part1
     (setq _part1 (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _part2
     (setq _part2 (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _part3
     (setq _part3 (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::camera_output :md5sum-) "f03ae215b98c387da3ca20f4eef0f6bf")
(setf (get assembly_control_ros::camera_output :datatype-) "assembly_control_ros/camera_output")
(setf (get assembly_control_ros::camera_output :definition-)
      "bool part_analyzed
bool part1
bool part2
bool part3
")



(provide :assembly_control_ros/camera_output "f03ae215b98c387da3ca20f4eef0f6bf")


