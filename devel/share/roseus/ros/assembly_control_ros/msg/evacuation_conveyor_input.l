;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::evacuation_conveyor_input)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'evacuation_conveyor_input (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::EVACUATION_CONVEYOR_INPUT")
  (make-package "ASSEMBLY_CONTROL_ROS::EVACUATION_CONVEYOR_INPUT"))

(in-package "ROS")
;;//! \htmlinclude evacuation_conveyor_input.msg.html


(defclass assembly_control_ros::evacuation_conveyor_input
  :super ros::object
  :slots (_ROB_AT_EC _ROB_RELEASED _restart_EC ))

(defmethod assembly_control_ros::evacuation_conveyor_input
  (:init
   (&key
    ((:ROB_AT_EC __ROB_AT_EC) nil)
    ((:ROB_RELEASED __ROB_RELEASED) nil)
    ((:restart_EC __restart_EC) nil)
    )
   (send-super :init)
   (setq _ROB_AT_EC __ROB_AT_EC)
   (setq _ROB_RELEASED __ROB_RELEASED)
   (setq _restart_EC __restart_EC)
   self)
  (:ROB_AT_EC
   (&optional __ROB_AT_EC)
   (if __ROB_AT_EC (setq _ROB_AT_EC __ROB_AT_EC)) _ROB_AT_EC)
  (:ROB_RELEASED
   (&optional __ROB_RELEASED)
   (if __ROB_RELEASED (setq _ROB_RELEASED __ROB_RELEASED)) _ROB_RELEASED)
  (:restart_EC
   (&optional __restart_EC)
   (if __restart_EC (setq _restart_EC __restart_EC)) _restart_EC)
  (:serialization-length
   ()
   (+
    ;; bool _ROB_AT_EC
    1
    ;; bool _ROB_RELEASED
    1
    ;; bool _restart_EC
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _ROB_AT_EC
       (if _ROB_AT_EC (write-byte -1 s) (write-byte 0 s))
     ;; bool _ROB_RELEASED
       (if _ROB_RELEASED (write-byte -1 s) (write-byte 0 s))
     ;; bool _restart_EC
       (if _restart_EC (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _ROB_AT_EC
     (setq _ROB_AT_EC (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _ROB_RELEASED
     (setq _ROB_RELEASED (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _restart_EC
     (setq _restart_EC (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::evacuation_conveyor_input :md5sum-) "f490813ad37f89aa0715c3042b565a3b")
(setf (get assembly_control_ros::evacuation_conveyor_input :datatype-) "assembly_control_ros/evacuation_conveyor_input")
(setf (get assembly_control_ros::evacuation_conveyor_input :definition-)
      "bool ROB_AT_EC
bool ROB_RELEASED
bool restart_EC
")



(provide :assembly_control_ros/evacuation_conveyor_input "f490813ad37f89aa0715c3042b565a3b")


