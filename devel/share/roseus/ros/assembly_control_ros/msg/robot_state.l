;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::robot_state)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'robot_state (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::ROBOT_STATE")
  (make-package "ASSEMBLY_CONTROL_ROS::ROBOT_STATE"))

(in-package "ROS")
;;//! \htmlinclude robot_state.msg.html


(defclass assembly_control_ros::robot_state
  :super ros::object
  :slots (_part_grasped _part_released _at_supply_conveyor _at_evacuation_conveyor _at_assembly_station _part1_assembled _part2_assembled _part3_assembled ))

(defmethod assembly_control_ros::robot_state
  (:init
   (&key
    ((:part_grasped __part_grasped) nil)
    ((:part_released __part_released) nil)
    ((:at_supply_conveyor __at_supply_conveyor) nil)
    ((:at_evacuation_conveyor __at_evacuation_conveyor) nil)
    ((:at_assembly_station __at_assembly_station) nil)
    ((:part1_assembled __part1_assembled) nil)
    ((:part2_assembled __part2_assembled) nil)
    ((:part3_assembled __part3_assembled) nil)
    )
   (send-super :init)
   (setq _part_grasped __part_grasped)
   (setq _part_released __part_released)
   (setq _at_supply_conveyor __at_supply_conveyor)
   (setq _at_evacuation_conveyor __at_evacuation_conveyor)
   (setq _at_assembly_station __at_assembly_station)
   (setq _part1_assembled __part1_assembled)
   (setq _part2_assembled __part2_assembled)
   (setq _part3_assembled __part3_assembled)
   self)
  (:part_grasped
   (&optional __part_grasped)
   (if __part_grasped (setq _part_grasped __part_grasped)) _part_grasped)
  (:part_released
   (&optional __part_released)
   (if __part_released (setq _part_released __part_released)) _part_released)
  (:at_supply_conveyor
   (&optional __at_supply_conveyor)
   (if __at_supply_conveyor (setq _at_supply_conveyor __at_supply_conveyor)) _at_supply_conveyor)
  (:at_evacuation_conveyor
   (&optional __at_evacuation_conveyor)
   (if __at_evacuation_conveyor (setq _at_evacuation_conveyor __at_evacuation_conveyor)) _at_evacuation_conveyor)
  (:at_assembly_station
   (&optional __at_assembly_station)
   (if __at_assembly_station (setq _at_assembly_station __at_assembly_station)) _at_assembly_station)
  (:part1_assembled
   (&optional __part1_assembled)
   (if __part1_assembled (setq _part1_assembled __part1_assembled)) _part1_assembled)
  (:part2_assembled
   (&optional __part2_assembled)
   (if __part2_assembled (setq _part2_assembled __part2_assembled)) _part2_assembled)
  (:part3_assembled
   (&optional __part3_assembled)
   (if __part3_assembled (setq _part3_assembled __part3_assembled)) _part3_assembled)
  (:serialization-length
   ()
   (+
    ;; bool _part_grasped
    1
    ;; bool _part_released
    1
    ;; bool _at_supply_conveyor
    1
    ;; bool _at_evacuation_conveyor
    1
    ;; bool _at_assembly_station
    1
    ;; bool _part1_assembled
    1
    ;; bool _part2_assembled
    1
    ;; bool _part3_assembled
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _part_grasped
       (if _part_grasped (write-byte -1 s) (write-byte 0 s))
     ;; bool _part_released
       (if _part_released (write-byte -1 s) (write-byte 0 s))
     ;; bool _at_supply_conveyor
       (if _at_supply_conveyor (write-byte -1 s) (write-byte 0 s))
     ;; bool _at_evacuation_conveyor
       (if _at_evacuation_conveyor (write-byte -1 s) (write-byte 0 s))
     ;; bool _at_assembly_station
       (if _at_assembly_station (write-byte -1 s) (write-byte 0 s))
     ;; bool _part1_assembled
       (if _part1_assembled (write-byte -1 s) (write-byte 0 s))
     ;; bool _part2_assembled
       (if _part2_assembled (write-byte -1 s) (write-byte 0 s))
     ;; bool _part3_assembled
       (if _part3_assembled (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _part_grasped
     (setq _part_grasped (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _part_released
     (setq _part_released (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _at_supply_conveyor
     (setq _at_supply_conveyor (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _at_evacuation_conveyor
     (setq _at_evacuation_conveyor (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _at_assembly_station
     (setq _at_assembly_station (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _part1_assembled
     (setq _part1_assembled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _part2_assembled
     (setq _part2_assembled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _part3_assembled
     (setq _part3_assembled (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::robot_state :md5sum-) "fc0a5945651ca653c6fce56198225a07")
(setf (get assembly_control_ros::robot_state :datatype-) "assembly_control_ros/robot_state")
(setf (get assembly_control_ros::robot_state :definition-)
      "bool part_grasped
bool part_released
bool at_supply_conveyor
bool at_evacuation_conveyor
bool at_assembly_station
bool part1_assembled
bool part2_assembled
bool part3_assembled
")



(provide :assembly_control_ros/robot_state "fc0a5945651ca653c6fce56198225a07")


