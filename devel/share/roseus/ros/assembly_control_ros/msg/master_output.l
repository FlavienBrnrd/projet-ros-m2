;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::master_output)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'master_output (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::MASTER_OUTPUT")
  (make-package "ASSEMBLY_CONTROL_ROS::MASTER_OUTPUT"))

(in-package "ROS")
;;//! \htmlinclude master_output.msg.html


(defclass assembly_control_ros::master_output
  :super ros::object
  :slots (_start_recognition ))

(defmethod assembly_control_ros::master_output
  (:init
   (&key
    ((:start_recognition __start_recognition) nil)
    )
   (send-super :init)
   (setq _start_recognition __start_recognition)
   self)
  (:start_recognition
   (&optional __start_recognition)
   (if __start_recognition (setq _start_recognition __start_recognition)) _start_recognition)
  (:serialization-length
   ()
   (+
    ;; bool _start_recognition
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _start_recognition
       (if _start_recognition (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _start_recognition
     (setq _start_recognition (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::master_output :md5sum-) "ce459b5e113c1fb16ff60052a520a482")
(setf (get assembly_control_ros::master_output :datatype-) "assembly_control_ros/master_output")
(setf (get assembly_control_ros::master_output :definition-)
      "bool start_recognition
")



(provide :assembly_control_ros/master_output "ce459b5e113c1fb16ff60052a520a482")


