;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::supply_conveyor_input)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'supply_conveyor_input (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::SUPPLY_CONVEYOR_INPUT")
  (make-package "ASSEMBLY_CONTROL_ROS::SUPPLY_CONVEYOR_INPUT"))

(in-package "ROS")
;;//! \htmlinclude supply_conveyor_input.msg.html


(defclass assembly_control_ros::supply_conveyor_input
  :super ros::object
  :slots (_restart ))

(defmethod assembly_control_ros::supply_conveyor_input
  (:init
   (&key
    ((:restart __restart) nil)
    )
   (send-super :init)
   (setq _restart __restart)
   self)
  (:restart
   (&optional __restart)
   (if __restart (setq _restart __restart)) _restart)
  (:serialization-length
   ()
   (+
    ;; bool _restart
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _restart
       (if _restart (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _restart
     (setq _restart (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::supply_conveyor_input :md5sum-) "b8cfd5cfa335ea9b5b24ce2c119bfcd6")
(setf (get assembly_control_ros::supply_conveyor_input :datatype-) "assembly_control_ros/supply_conveyor_input")
(setf (get assembly_control_ros::supply_conveyor_input :definition-)
      "bool restart
")



(provide :assembly_control_ros/supply_conveyor_input "b8cfd5cfa335ea9b5b24ce2c119bfcd6")


