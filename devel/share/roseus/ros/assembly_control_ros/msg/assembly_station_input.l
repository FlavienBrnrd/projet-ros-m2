;; Auto-generated. Do not edit!


(when (boundp 'assembly_control_ros::assembly_station_input)
  (if (not (find-package "ASSEMBLY_CONTROL_ROS"))
    (make-package "ASSEMBLY_CONTROL_ROS"))
  (shadow 'assembly_station_input (find-package "ASSEMBLY_CONTROL_ROS")))
(unless (find-package "ASSEMBLY_CONTROL_ROS::ASSEMBLY_STATION_INPUT")
  (make-package "ASSEMBLY_CONTROL_ROS::ASSEMBLY_STATION_INPUT"))

(in-package "ROS")
;;//! \htmlinclude assembly_station_input.msg.html


(defclass assembly_control_ros::assembly_station_input
  :super ros::object
  :slots (_PARTS_DONE ))

(defmethod assembly_control_ros::assembly_station_input
  (:init
   (&key
    ((:PARTS_DONE __PARTS_DONE) nil)
    )
   (send-super :init)
   (setq _PARTS_DONE __PARTS_DONE)
   self)
  (:PARTS_DONE
   (&optional __PARTS_DONE)
   (if __PARTS_DONE (setq _PARTS_DONE __PARTS_DONE)) _PARTS_DONE)
  (:serialization-length
   ()
   (+
    ;; bool _PARTS_DONE
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _PARTS_DONE
       (if _PARTS_DONE (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _PARTS_DONE
     (setq _PARTS_DONE (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get assembly_control_ros::assembly_station_input :md5sum-) "f6176f35c766c927d57bbd15eeee6d4b")
(setf (get assembly_control_ros::assembly_station_input :datatype-) "assembly_control_ros/assembly_station_input")
(setf (get assembly_control_ros::assembly_station_input :definition-)
      "
bool PARTS_DONE
")



(provide :assembly_control_ros/assembly_station_input "f6176f35c766c927d57bbd15eeee6d4b")


