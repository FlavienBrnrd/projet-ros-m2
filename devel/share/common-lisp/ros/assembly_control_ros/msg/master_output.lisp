; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude master_output.msg.html

(cl:defclass <master_output> (roslisp-msg-protocol:ros-message)
  ((start_recognition
    :reader start_recognition
    :initarg :start_recognition
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass master_output (<master_output>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <master_output>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'master_output)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<master_output> is deprecated: use assembly_control_ros-msg:master_output instead.")))

(cl:ensure-generic-function 'start_recognition-val :lambda-list '(m))
(cl:defmethod start_recognition-val ((m <master_output>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:start_recognition-val is deprecated.  Use assembly_control_ros-msg:start_recognition instead.")
  (start_recognition m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <master_output>) ostream)
  "Serializes a message object of type '<master_output>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'start_recognition) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <master_output>) istream)
  "Deserializes a message object of type '<master_output>"
    (cl:setf (cl:slot-value msg 'start_recognition) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<master_output>)))
  "Returns string type for a message object of type '<master_output>"
  "assembly_control_ros/master_output")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'master_output)))
  "Returns string type for a message object of type 'master_output"
  "assembly_control_ros/master_output")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<master_output>)))
  "Returns md5sum for a message object of type '<master_output>"
  "ce459b5e113c1fb16ff60052a520a482")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'master_output)))
  "Returns md5sum for a message object of type 'master_output"
  "ce459b5e113c1fb16ff60052a520a482")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<master_output>)))
  "Returns full string definition for message of type '<master_output>"
  (cl:format cl:nil "bool start_recognition~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'master_output)))
  "Returns full string definition for message of type 'master_output"
  (cl:format cl:nil "bool start_recognition~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <master_output>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <master_output>))
  "Converts a ROS message object to a list"
  (cl:list 'master_output
    (cl:cons ':start_recognition (start_recognition msg))
))
