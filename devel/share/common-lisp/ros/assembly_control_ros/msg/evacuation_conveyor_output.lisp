; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude evacuation_conveyor_output.msg.html

(cl:defclass <evacuation_conveyor_output> (roslisp-msg-protocol:ros-message)
  ((EC_STOPPED
    :reader EC_STOPPED
    :initarg :EC_STOPPED
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass evacuation_conveyor_output (<evacuation_conveyor_output>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <evacuation_conveyor_output>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'evacuation_conveyor_output)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<evacuation_conveyor_output> is deprecated: use assembly_control_ros-msg:evacuation_conveyor_output instead.")))

(cl:ensure-generic-function 'EC_STOPPED-val :lambda-list '(m))
(cl:defmethod EC_STOPPED-val ((m <evacuation_conveyor_output>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:EC_STOPPED-val is deprecated.  Use assembly_control_ros-msg:EC_STOPPED instead.")
  (EC_STOPPED m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <evacuation_conveyor_output>) ostream)
  "Serializes a message object of type '<evacuation_conveyor_output>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'EC_STOPPED) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <evacuation_conveyor_output>) istream)
  "Deserializes a message object of type '<evacuation_conveyor_output>"
    (cl:setf (cl:slot-value msg 'EC_STOPPED) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<evacuation_conveyor_output>)))
  "Returns string type for a message object of type '<evacuation_conveyor_output>"
  "assembly_control_ros/evacuation_conveyor_output")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'evacuation_conveyor_output)))
  "Returns string type for a message object of type 'evacuation_conveyor_output"
  "assembly_control_ros/evacuation_conveyor_output")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<evacuation_conveyor_output>)))
  "Returns md5sum for a message object of type '<evacuation_conveyor_output>"
  "c09c96dee6aef70265cad5be94ee4a4d")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'evacuation_conveyor_output)))
  "Returns md5sum for a message object of type 'evacuation_conveyor_output"
  "c09c96dee6aef70265cad5be94ee4a4d")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<evacuation_conveyor_output>)))
  "Returns full string definition for message of type '<evacuation_conveyor_output>"
  (cl:format cl:nil "bool EC_STOPPED~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'evacuation_conveyor_output)))
  "Returns full string definition for message of type 'evacuation_conveyor_output"
  (cl:format cl:nil "bool EC_STOPPED~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <evacuation_conveyor_output>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <evacuation_conveyor_output>))
  "Converts a ROS message object to a list"
  (cl:list 'evacuation_conveyor_output
    (cl:cons ':EC_STOPPED (EC_STOPPED msg))
))
