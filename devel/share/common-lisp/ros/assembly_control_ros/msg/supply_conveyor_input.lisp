; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude supply_conveyor_input.msg.html

(cl:defclass <supply_conveyor_input> (roslisp-msg-protocol:ros-message)
  ((restart
    :reader restart
    :initarg :restart
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass supply_conveyor_input (<supply_conveyor_input>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <supply_conveyor_input>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'supply_conveyor_input)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<supply_conveyor_input> is deprecated: use assembly_control_ros-msg:supply_conveyor_input instead.")))

(cl:ensure-generic-function 'restart-val :lambda-list '(m))
(cl:defmethod restart-val ((m <supply_conveyor_input>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:restart-val is deprecated.  Use assembly_control_ros-msg:restart instead.")
  (restart m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <supply_conveyor_input>) ostream)
  "Serializes a message object of type '<supply_conveyor_input>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'restart) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <supply_conveyor_input>) istream)
  "Deserializes a message object of type '<supply_conveyor_input>"
    (cl:setf (cl:slot-value msg 'restart) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<supply_conveyor_input>)))
  "Returns string type for a message object of type '<supply_conveyor_input>"
  "assembly_control_ros/supply_conveyor_input")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'supply_conveyor_input)))
  "Returns string type for a message object of type 'supply_conveyor_input"
  "assembly_control_ros/supply_conveyor_input")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<supply_conveyor_input>)))
  "Returns md5sum for a message object of type '<supply_conveyor_input>"
  "b8cfd5cfa335ea9b5b24ce2c119bfcd6")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'supply_conveyor_input)))
  "Returns md5sum for a message object of type 'supply_conveyor_input"
  "b8cfd5cfa335ea9b5b24ce2c119bfcd6")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<supply_conveyor_input>)))
  "Returns full string definition for message of type '<supply_conveyor_input>"
  (cl:format cl:nil "bool restart~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'supply_conveyor_input)))
  "Returns full string definition for message of type 'supply_conveyor_input"
  (cl:format cl:nil "bool restart~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <supply_conveyor_input>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <supply_conveyor_input>))
  "Converts a ROS message object to a list"
  (cl:list 'supply_conveyor_input
    (cl:cons ':restart (restart msg))
))
