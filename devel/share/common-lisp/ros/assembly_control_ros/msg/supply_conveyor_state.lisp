; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude supply_conveyor_state.msg.html

(cl:defclass <supply_conveyor_state> (roslisp-msg-protocol:ros-message)
  ((optical_barrier
    :reader optical_barrier
    :initarg :optical_barrier
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass supply_conveyor_state (<supply_conveyor_state>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <supply_conveyor_state>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'supply_conveyor_state)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<supply_conveyor_state> is deprecated: use assembly_control_ros-msg:supply_conveyor_state instead.")))

(cl:ensure-generic-function 'optical_barrier-val :lambda-list '(m))
(cl:defmethod optical_barrier-val ((m <supply_conveyor_state>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:optical_barrier-val is deprecated.  Use assembly_control_ros-msg:optical_barrier instead.")
  (optical_barrier m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <supply_conveyor_state>) ostream)
  "Serializes a message object of type '<supply_conveyor_state>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'optical_barrier) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <supply_conveyor_state>) istream)
  "Deserializes a message object of type '<supply_conveyor_state>"
    (cl:setf (cl:slot-value msg 'optical_barrier) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<supply_conveyor_state>)))
  "Returns string type for a message object of type '<supply_conveyor_state>"
  "assembly_control_ros/supply_conveyor_state")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'supply_conveyor_state)))
  "Returns string type for a message object of type 'supply_conveyor_state"
  "assembly_control_ros/supply_conveyor_state")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<supply_conveyor_state>)))
  "Returns md5sum for a message object of type '<supply_conveyor_state>"
  "36bbab7385881955fe117c4d8550a944")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'supply_conveyor_state)))
  "Returns md5sum for a message object of type 'supply_conveyor_state"
  "36bbab7385881955fe117c4d8550a944")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<supply_conveyor_state>)))
  "Returns full string definition for message of type '<supply_conveyor_state>"
  (cl:format cl:nil "bool optical_barrier~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'supply_conveyor_state)))
  "Returns full string definition for message of type 'supply_conveyor_state"
  (cl:format cl:nil "bool optical_barrier~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <supply_conveyor_state>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <supply_conveyor_state>))
  "Converts a ROS message object to a list"
  (cl:list 'supply_conveyor_state
    (cl:cons ':optical_barrier (optical_barrier msg))
))
