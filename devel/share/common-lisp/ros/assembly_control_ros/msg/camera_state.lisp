; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude camera_state.msg.html

(cl:defclass <camera_state> (roslisp-msg-protocol:ros-message)
  ((done
    :reader done
    :initarg :done
    :type cl:boolean
    :initform cl:nil)
   (part
    :reader part
    :initarg :part
    :type cl:fixnum
    :initform 0))
)

(cl:defclass camera_state (<camera_state>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <camera_state>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'camera_state)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<camera_state> is deprecated: use assembly_control_ros-msg:camera_state instead.")))

(cl:ensure-generic-function 'done-val :lambda-list '(m))
(cl:defmethod done-val ((m <camera_state>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:done-val is deprecated.  Use assembly_control_ros-msg:done instead.")
  (done m))

(cl:ensure-generic-function 'part-val :lambda-list '(m))
(cl:defmethod part-val ((m <camera_state>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:part-val is deprecated.  Use assembly_control_ros-msg:part instead.")
  (part m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <camera_state>) ostream)
  "Serializes a message object of type '<camera_state>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'done) 1 0)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'part)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 256) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <camera_state>) istream)
  "Deserializes a message object of type '<camera_state>"
    (cl:setf (cl:slot-value msg 'done) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'part) (cl:if (cl:< unsigned 128) unsigned (cl:- unsigned 256))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<camera_state>)))
  "Returns string type for a message object of type '<camera_state>"
  "assembly_control_ros/camera_state")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'camera_state)))
  "Returns string type for a message object of type 'camera_state"
  "assembly_control_ros/camera_state")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<camera_state>)))
  "Returns md5sum for a message object of type '<camera_state>"
  "e285620228be6451d908510458fc988c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'camera_state)))
  "Returns md5sum for a message object of type 'camera_state"
  "e285620228be6451d908510458fc988c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<camera_state>)))
  "Returns full string definition for message of type '<camera_state>"
  (cl:format cl:nil "bool done~%int8 part~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'camera_state)))
  "Returns full string definition for message of type 'camera_state"
  (cl:format cl:nil "bool done~%int8 part~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <camera_state>))
  (cl:+ 0
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <camera_state>))
  "Converts a ROS message object to a list"
  (cl:list 'camera_state
    (cl:cons ':done (done msg))
    (cl:cons ':part (part msg))
))
