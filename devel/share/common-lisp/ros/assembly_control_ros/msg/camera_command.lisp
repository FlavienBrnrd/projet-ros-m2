; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude camera_command.msg.html

(cl:defclass <camera_command> (roslisp-msg-protocol:ros-message)
  ((process
    :reader process
    :initarg :process
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass camera_command (<camera_command>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <camera_command>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'camera_command)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<camera_command> is deprecated: use assembly_control_ros-msg:camera_command instead.")))

(cl:ensure-generic-function 'process-val :lambda-list '(m))
(cl:defmethod process-val ((m <camera_command>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:process-val is deprecated.  Use assembly_control_ros-msg:process instead.")
  (process m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <camera_command>) ostream)
  "Serializes a message object of type '<camera_command>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'process) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <camera_command>) istream)
  "Deserializes a message object of type '<camera_command>"
    (cl:setf (cl:slot-value msg 'process) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<camera_command>)))
  "Returns string type for a message object of type '<camera_command>"
  "assembly_control_ros/camera_command")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'camera_command)))
  "Returns string type for a message object of type 'camera_command"
  "assembly_control_ros/camera_command")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<camera_command>)))
  "Returns md5sum for a message object of type '<camera_command>"
  "1c650d1d3c30589cc2658a353420cbf2")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'camera_command)))
  "Returns md5sum for a message object of type 'camera_command"
  "1c650d1d3c30589cc2658a353420cbf2")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<camera_command>)))
  "Returns full string definition for message of type '<camera_command>"
  (cl:format cl:nil "bool process~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'camera_command)))
  "Returns full string definition for message of type 'camera_command"
  (cl:format cl:nil "bool process~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <camera_command>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <camera_command>))
  "Converts a ROS message object to a list"
  (cl:list 'camera_command
    (cl:cons ':process (process msg))
))
