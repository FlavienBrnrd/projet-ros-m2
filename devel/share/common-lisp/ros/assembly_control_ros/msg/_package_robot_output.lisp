(cl:in-package assembly_control_ros-msg)
(cl:export '(GO_LEFT_DONE-VAL
          GO_LEFT_DONE
          GO_RIGHT_DONE-VAL
          GO_RIGHT_DONE
          GO_GRASP_DONE-VAL
          GO_GRASP_DONE
          GO_RELEASE_DONE-VAL
          GO_RELEASE_DONE
          ASSEMBLE_1_DONE-VAL
          ASSEMBLE_1_DONE
          ASSEMBLE_2_DONE-VAL
          ASSEMBLE_2_DONE
          ASSEMBLE_3_DONE-VAL
          ASSEMBLE_3_DONE
))