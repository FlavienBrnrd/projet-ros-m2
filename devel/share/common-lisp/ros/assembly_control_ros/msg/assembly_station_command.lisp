; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude assembly_station_command.msg.html

(cl:defclass <assembly_station_command> (roslisp-msg-protocol:ros-message)
  ((check
    :reader check
    :initarg :check
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass assembly_station_command (<assembly_station_command>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <assembly_station_command>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'assembly_station_command)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<assembly_station_command> is deprecated: use assembly_control_ros-msg:assembly_station_command instead.")))

(cl:ensure-generic-function 'check-val :lambda-list '(m))
(cl:defmethod check-val ((m <assembly_station_command>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:check-val is deprecated.  Use assembly_control_ros-msg:check instead.")
  (check m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <assembly_station_command>) ostream)
  "Serializes a message object of type '<assembly_station_command>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'check) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <assembly_station_command>) istream)
  "Deserializes a message object of type '<assembly_station_command>"
    (cl:setf (cl:slot-value msg 'check) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<assembly_station_command>)))
  "Returns string type for a message object of type '<assembly_station_command>"
  "assembly_control_ros/assembly_station_command")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'assembly_station_command)))
  "Returns string type for a message object of type 'assembly_station_command"
  "assembly_control_ros/assembly_station_command")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<assembly_station_command>)))
  "Returns md5sum for a message object of type '<assembly_station_command>"
  "c5df00fea9d1f39520fa0345cbde1b26")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'assembly_station_command)))
  "Returns md5sum for a message object of type 'assembly_station_command"
  "c5df00fea9d1f39520fa0345cbde1b26")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<assembly_station_command>)))
  "Returns full string definition for message of type '<assembly_station_command>"
  (cl:format cl:nil "bool check~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'assembly_station_command)))
  "Returns full string definition for message of type 'assembly_station_command"
  (cl:format cl:nil "bool check~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <assembly_station_command>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <assembly_station_command>))
  "Converts a ROS message object to a list"
  (cl:list 'assembly_station_command
    (cl:cons ':check (check msg))
))
