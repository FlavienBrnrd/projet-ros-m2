; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude robot_output.msg.html

(cl:defclass <robot_output> (roslisp-msg-protocol:ros-message)
  ((go_left_done
    :reader go_left_done
    :initarg :go_left_done
    :type cl:boolean
    :initform cl:nil)
   (go_right_done
    :reader go_right_done
    :initarg :go_right_done
    :type cl:boolean
    :initform cl:nil)
   (go_grasp_done
    :reader go_grasp_done
    :initarg :go_grasp_done
    :type cl:boolean
    :initform cl:nil)
   (go_release_done
    :reader go_release_done
    :initarg :go_release_done
    :type cl:boolean
    :initform cl:nil)
   (assemble_1_done
    :reader assemble_1_done
    :initarg :assemble_1_done
    :type cl:boolean
    :initform cl:nil)
   (assemble_2_done
    :reader assemble_2_done
    :initarg :assemble_2_done
    :type cl:boolean
    :initform cl:nil)
   (assemble_3_done
    :reader assemble_3_done
    :initarg :assemble_3_done
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass robot_output (<robot_output>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <robot_output>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'robot_output)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<robot_output> is deprecated: use assembly_control_ros-msg:robot_output instead.")))

(cl:ensure-generic-function 'go_left_done-val :lambda-list '(m))
(cl:defmethod go_left_done-val ((m <robot_output>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:go_left_done-val is deprecated.  Use assembly_control_ros-msg:go_left_done instead.")
  (go_left_done m))

(cl:ensure-generic-function 'go_right_done-val :lambda-list '(m))
(cl:defmethod go_right_done-val ((m <robot_output>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:go_right_done-val is deprecated.  Use assembly_control_ros-msg:go_right_done instead.")
  (go_right_done m))

(cl:ensure-generic-function 'go_grasp_done-val :lambda-list '(m))
(cl:defmethod go_grasp_done-val ((m <robot_output>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:go_grasp_done-val is deprecated.  Use assembly_control_ros-msg:go_grasp_done instead.")
  (go_grasp_done m))

(cl:ensure-generic-function 'go_release_done-val :lambda-list '(m))
(cl:defmethod go_release_done-val ((m <robot_output>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:go_release_done-val is deprecated.  Use assembly_control_ros-msg:go_release_done instead.")
  (go_release_done m))

(cl:ensure-generic-function 'assemble_1_done-val :lambda-list '(m))
(cl:defmethod assemble_1_done-val ((m <robot_output>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:assemble_1_done-val is deprecated.  Use assembly_control_ros-msg:assemble_1_done instead.")
  (assemble_1_done m))

(cl:ensure-generic-function 'assemble_2_done-val :lambda-list '(m))
(cl:defmethod assemble_2_done-val ((m <robot_output>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:assemble_2_done-val is deprecated.  Use assembly_control_ros-msg:assemble_2_done instead.")
  (assemble_2_done m))

(cl:ensure-generic-function 'assemble_3_done-val :lambda-list '(m))
(cl:defmethod assemble_3_done-val ((m <robot_output>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:assemble_3_done-val is deprecated.  Use assembly_control_ros-msg:assemble_3_done instead.")
  (assemble_3_done m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <robot_output>) ostream)
  "Serializes a message object of type '<robot_output>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'go_left_done) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'go_right_done) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'go_grasp_done) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'go_release_done) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'assemble_1_done) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'assemble_2_done) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'assemble_3_done) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <robot_output>) istream)
  "Deserializes a message object of type '<robot_output>"
    (cl:setf (cl:slot-value msg 'go_left_done) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'go_right_done) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'go_grasp_done) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'go_release_done) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'assemble_1_done) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'assemble_2_done) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'assemble_3_done) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<robot_output>)))
  "Returns string type for a message object of type '<robot_output>"
  "assembly_control_ros/robot_output")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'robot_output)))
  "Returns string type for a message object of type 'robot_output"
  "assembly_control_ros/robot_output")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<robot_output>)))
  "Returns md5sum for a message object of type '<robot_output>"
  "f860ddfdf175592b078c56f3a3fb1cee")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'robot_output)))
  "Returns md5sum for a message object of type 'robot_output"
  "f860ddfdf175592b078c56f3a3fb1cee")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<robot_output>)))
  "Returns full string definition for message of type '<robot_output>"
  (cl:format cl:nil "bool go_left_done~%bool go_right_done~%bool go_grasp_done~%bool go_release_done~%bool assemble_1_done~%bool assemble_2_done~%bool assemble_3_done~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'robot_output)))
  "Returns full string definition for message of type 'robot_output"
  (cl:format cl:nil "bool go_left_done~%bool go_right_done~%bool go_grasp_done~%bool go_release_done~%bool assemble_1_done~%bool assemble_2_done~%bool assemble_3_done~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <robot_output>))
  (cl:+ 0
     1
     1
     1
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <robot_output>))
  "Converts a ROS message object to a list"
  (cl:list 'robot_output
    (cl:cons ':go_left_done (go_left_done msg))
    (cl:cons ':go_right_done (go_right_done msg))
    (cl:cons ':go_grasp_done (go_grasp_done msg))
    (cl:cons ':go_release_done (go_release_done msg))
    (cl:cons ':assemble_1_done (assemble_1_done msg))
    (cl:cons ':assemble_2_done (assemble_2_done msg))
    (cl:cons ':assemble_3_done (assemble_3_done msg))
))
