; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude assembly_station_state.msg.html

(cl:defclass <assembly_station_state> (roslisp-msg-protocol:ros-message)
  ((valid
    :reader valid
    :initarg :valid
    :type cl:boolean
    :initform cl:nil)
   (evacuated
    :reader evacuated
    :initarg :evacuated
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass assembly_station_state (<assembly_station_state>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <assembly_station_state>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'assembly_station_state)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<assembly_station_state> is deprecated: use assembly_control_ros-msg:assembly_station_state instead.")))

(cl:ensure-generic-function 'valid-val :lambda-list '(m))
(cl:defmethod valid-val ((m <assembly_station_state>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:valid-val is deprecated.  Use assembly_control_ros-msg:valid instead.")
  (valid m))

(cl:ensure-generic-function 'evacuated-val :lambda-list '(m))
(cl:defmethod evacuated-val ((m <assembly_station_state>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:evacuated-val is deprecated.  Use assembly_control_ros-msg:evacuated instead.")
  (evacuated m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <assembly_station_state>) ostream)
  "Serializes a message object of type '<assembly_station_state>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'valid) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'evacuated) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <assembly_station_state>) istream)
  "Deserializes a message object of type '<assembly_station_state>"
    (cl:setf (cl:slot-value msg 'valid) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'evacuated) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<assembly_station_state>)))
  "Returns string type for a message object of type '<assembly_station_state>"
  "assembly_control_ros/assembly_station_state")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'assembly_station_state)))
  "Returns string type for a message object of type 'assembly_station_state"
  "assembly_control_ros/assembly_station_state")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<assembly_station_state>)))
  "Returns md5sum for a message object of type '<assembly_station_state>"
  "5a5113dbe54fee79575bff785c5ca0bc")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'assembly_station_state)))
  "Returns md5sum for a message object of type 'assembly_station_state"
  "5a5113dbe54fee79575bff785c5ca0bc")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<assembly_station_state>)))
  "Returns full string definition for message of type '<assembly_station_state>"
  (cl:format cl:nil "bool valid~%bool evacuated~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'assembly_station_state)))
  "Returns full string definition for message of type 'assembly_station_state"
  (cl:format cl:nil "bool valid~%bool evacuated~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <assembly_station_state>))
  (cl:+ 0
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <assembly_station_state>))
  "Converts a ROS message object to a list"
  (cl:list 'assembly_station_state
    (cl:cons ':valid (valid msg))
    (cl:cons ':evacuated (evacuated msg))
))
