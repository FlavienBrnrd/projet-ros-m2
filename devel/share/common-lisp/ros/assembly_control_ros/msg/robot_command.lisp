; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude robot_command.msg.html

(cl:defclass <robot_command> (roslisp-msg-protocol:ros-message)
  ((move_right
    :reader move_right
    :initarg :move_right
    :type cl:boolean
    :initform cl:nil)
   (move_left
    :reader move_left
    :initarg :move_left
    :type cl:boolean
    :initform cl:nil)
   (grasp
    :reader grasp
    :initarg :grasp
    :type cl:boolean
    :initform cl:nil)
   (release
    :reader release
    :initarg :release
    :type cl:boolean
    :initform cl:nil)
   (assemble_part1
    :reader assemble_part1
    :initarg :assemble_part1
    :type cl:boolean
    :initform cl:nil)
   (assemble_part2
    :reader assemble_part2
    :initarg :assemble_part2
    :type cl:boolean
    :initform cl:nil)
   (assemble_part3
    :reader assemble_part3
    :initarg :assemble_part3
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass robot_command (<robot_command>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <robot_command>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'robot_command)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<robot_command> is deprecated: use assembly_control_ros-msg:robot_command instead.")))

(cl:ensure-generic-function 'move_right-val :lambda-list '(m))
(cl:defmethod move_right-val ((m <robot_command>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:move_right-val is deprecated.  Use assembly_control_ros-msg:move_right instead.")
  (move_right m))

(cl:ensure-generic-function 'move_left-val :lambda-list '(m))
(cl:defmethod move_left-val ((m <robot_command>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:move_left-val is deprecated.  Use assembly_control_ros-msg:move_left instead.")
  (move_left m))

(cl:ensure-generic-function 'grasp-val :lambda-list '(m))
(cl:defmethod grasp-val ((m <robot_command>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:grasp-val is deprecated.  Use assembly_control_ros-msg:grasp instead.")
  (grasp m))

(cl:ensure-generic-function 'release-val :lambda-list '(m))
(cl:defmethod release-val ((m <robot_command>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:release-val is deprecated.  Use assembly_control_ros-msg:release instead.")
  (release m))

(cl:ensure-generic-function 'assemble_part1-val :lambda-list '(m))
(cl:defmethod assemble_part1-val ((m <robot_command>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:assemble_part1-val is deprecated.  Use assembly_control_ros-msg:assemble_part1 instead.")
  (assemble_part1 m))

(cl:ensure-generic-function 'assemble_part2-val :lambda-list '(m))
(cl:defmethod assemble_part2-val ((m <robot_command>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:assemble_part2-val is deprecated.  Use assembly_control_ros-msg:assemble_part2 instead.")
  (assemble_part2 m))

(cl:ensure-generic-function 'assemble_part3-val :lambda-list '(m))
(cl:defmethod assemble_part3-val ((m <robot_command>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:assemble_part3-val is deprecated.  Use assembly_control_ros-msg:assemble_part3 instead.")
  (assemble_part3 m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <robot_command>) ostream)
  "Serializes a message object of type '<robot_command>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'move_right) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'move_left) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'grasp) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'release) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'assemble_part1) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'assemble_part2) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'assemble_part3) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <robot_command>) istream)
  "Deserializes a message object of type '<robot_command>"
    (cl:setf (cl:slot-value msg 'move_right) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'move_left) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'grasp) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'release) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'assemble_part1) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'assemble_part2) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'assemble_part3) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<robot_command>)))
  "Returns string type for a message object of type '<robot_command>"
  "assembly_control_ros/robot_command")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'robot_command)))
  "Returns string type for a message object of type 'robot_command"
  "assembly_control_ros/robot_command")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<robot_command>)))
  "Returns md5sum for a message object of type '<robot_command>"
  "90dd75667caf471745cdc1c8deb81f71")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'robot_command)))
  "Returns md5sum for a message object of type 'robot_command"
  "90dd75667caf471745cdc1c8deb81f71")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<robot_command>)))
  "Returns full string definition for message of type '<robot_command>"
  (cl:format cl:nil "bool move_right~%bool move_left~%bool grasp~%bool release~%bool assemble_part1~%bool assemble_part2~%bool assemble_part3~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'robot_command)))
  "Returns full string definition for message of type 'robot_command"
  (cl:format cl:nil "bool move_right~%bool move_left~%bool grasp~%bool release~%bool assemble_part1~%bool assemble_part2~%bool assemble_part3~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <robot_command>))
  (cl:+ 0
     1
     1
     1
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <robot_command>))
  "Converts a ROS message object to a list"
  (cl:list 'robot_command
    (cl:cons ':move_right (move_right msg))
    (cl:cons ':move_left (move_left msg))
    (cl:cons ':grasp (grasp msg))
    (cl:cons ':release (release msg))
    (cl:cons ':assemble_part1 (assemble_part1 msg))
    (cl:cons ':assemble_part2 (assemble_part2 msg))
    (cl:cons ':assemble_part3 (assemble_part3 msg))
))
