; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude assembly_station_input.msg.html

(cl:defclass <assembly_station_input> (roslisp-msg-protocol:ros-message)
  ((PARTS_DONE
    :reader PARTS_DONE
    :initarg :PARTS_DONE
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass assembly_station_input (<assembly_station_input>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <assembly_station_input>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'assembly_station_input)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<assembly_station_input> is deprecated: use assembly_control_ros-msg:assembly_station_input instead.")))

(cl:ensure-generic-function 'PARTS_DONE-val :lambda-list '(m))
(cl:defmethod PARTS_DONE-val ((m <assembly_station_input>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:PARTS_DONE-val is deprecated.  Use assembly_control_ros-msg:PARTS_DONE instead.")
  (PARTS_DONE m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <assembly_station_input>) ostream)
  "Serializes a message object of type '<assembly_station_input>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'PARTS_DONE) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <assembly_station_input>) istream)
  "Deserializes a message object of type '<assembly_station_input>"
    (cl:setf (cl:slot-value msg 'PARTS_DONE) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<assembly_station_input>)))
  "Returns string type for a message object of type '<assembly_station_input>"
  "assembly_control_ros/assembly_station_input")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'assembly_station_input)))
  "Returns string type for a message object of type 'assembly_station_input"
  "assembly_control_ros/assembly_station_input")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<assembly_station_input>)))
  "Returns md5sum for a message object of type '<assembly_station_input>"
  "f6176f35c766c927d57bbd15eeee6d4b")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'assembly_station_input)))
  "Returns md5sum for a message object of type 'assembly_station_input"
  "f6176f35c766c927d57bbd15eeee6d4b")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<assembly_station_input>)))
  "Returns full string definition for message of type '<assembly_station_input>"
  (cl:format cl:nil "~%bool PARTS_DONE~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'assembly_station_input)))
  "Returns full string definition for message of type 'assembly_station_input"
  (cl:format cl:nil "~%bool PARTS_DONE~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <assembly_station_input>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <assembly_station_input>))
  "Converts a ROS message object to a list"
  (cl:list 'assembly_station_input
    (cl:cons ':PARTS_DONE (PARTS_DONE msg))
))
