; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude camera_output.msg.html

(cl:defclass <camera_output> (roslisp-msg-protocol:ros-message)
  ((part_analyzed
    :reader part_analyzed
    :initarg :part_analyzed
    :type cl:boolean
    :initform cl:nil)
   (part1
    :reader part1
    :initarg :part1
    :type cl:boolean
    :initform cl:nil)
   (part2
    :reader part2
    :initarg :part2
    :type cl:boolean
    :initform cl:nil)
   (part3
    :reader part3
    :initarg :part3
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass camera_output (<camera_output>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <camera_output>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'camera_output)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<camera_output> is deprecated: use assembly_control_ros-msg:camera_output instead.")))

(cl:ensure-generic-function 'part_analyzed-val :lambda-list '(m))
(cl:defmethod part_analyzed-val ((m <camera_output>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:part_analyzed-val is deprecated.  Use assembly_control_ros-msg:part_analyzed instead.")
  (part_analyzed m))

(cl:ensure-generic-function 'part1-val :lambda-list '(m))
(cl:defmethod part1-val ((m <camera_output>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:part1-val is deprecated.  Use assembly_control_ros-msg:part1 instead.")
  (part1 m))

(cl:ensure-generic-function 'part2-val :lambda-list '(m))
(cl:defmethod part2-val ((m <camera_output>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:part2-val is deprecated.  Use assembly_control_ros-msg:part2 instead.")
  (part2 m))

(cl:ensure-generic-function 'part3-val :lambda-list '(m))
(cl:defmethod part3-val ((m <camera_output>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:part3-val is deprecated.  Use assembly_control_ros-msg:part3 instead.")
  (part3 m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <camera_output>) ostream)
  "Serializes a message object of type '<camera_output>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'part_analyzed) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'part1) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'part2) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'part3) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <camera_output>) istream)
  "Deserializes a message object of type '<camera_output>"
    (cl:setf (cl:slot-value msg 'part_analyzed) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'part1) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'part2) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'part3) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<camera_output>)))
  "Returns string type for a message object of type '<camera_output>"
  "assembly_control_ros/camera_output")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'camera_output)))
  "Returns string type for a message object of type 'camera_output"
  "assembly_control_ros/camera_output")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<camera_output>)))
  "Returns md5sum for a message object of type '<camera_output>"
  "f03ae215b98c387da3ca20f4eef0f6bf")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'camera_output)))
  "Returns md5sum for a message object of type 'camera_output"
  "f03ae215b98c387da3ca20f4eef0f6bf")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<camera_output>)))
  "Returns full string definition for message of type '<camera_output>"
  (cl:format cl:nil "bool part_analyzed~%bool part1~%bool part2~%bool part3~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'camera_output)))
  "Returns full string definition for message of type 'camera_output"
  (cl:format cl:nil "bool part_analyzed~%bool part1~%bool part2~%bool part3~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <camera_output>))
  (cl:+ 0
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <camera_output>))
  "Converts a ROS message object to a list"
  (cl:list 'camera_output
    (cl:cons ':part_analyzed (part_analyzed msg))
    (cl:cons ':part1 (part1 msg))
    (cl:cons ':part2 (part2 msg))
    (cl:cons ':part3 (part3 msg))
))
