(cl:in-package assembly_control_ros-msg)
(cl:export '(MOVE_RIGHT-VAL
          MOVE_RIGHT
          MOVE_LEFT-VAL
          MOVE_LEFT
          GRASP-VAL
          GRASP
          RELEASE-VAL
          RELEASE
          ASSEMBLE_PART1-VAL
          ASSEMBLE_PART1
          ASSEMBLE_PART2-VAL
          ASSEMBLE_PART2
          ASSEMBLE_PART3-VAL
          ASSEMBLE_PART3
))