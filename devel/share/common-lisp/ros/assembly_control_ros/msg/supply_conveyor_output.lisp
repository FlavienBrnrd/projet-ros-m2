; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude supply_conveyor_output.msg.html

(cl:defclass <supply_conveyor_output> (roslisp-msg-protocol:ros-message)
  ((part_available
    :reader part_available
    :initarg :part_available
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass supply_conveyor_output (<supply_conveyor_output>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <supply_conveyor_output>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'supply_conveyor_output)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<supply_conveyor_output> is deprecated: use assembly_control_ros-msg:supply_conveyor_output instead.")))

(cl:ensure-generic-function 'part_available-val :lambda-list '(m))
(cl:defmethod part_available-val ((m <supply_conveyor_output>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:part_available-val is deprecated.  Use assembly_control_ros-msg:part_available instead.")
  (part_available m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <supply_conveyor_output>) ostream)
  "Serializes a message object of type '<supply_conveyor_output>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'part_available) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <supply_conveyor_output>) istream)
  "Deserializes a message object of type '<supply_conveyor_output>"
    (cl:setf (cl:slot-value msg 'part_available) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<supply_conveyor_output>)))
  "Returns string type for a message object of type '<supply_conveyor_output>"
  "assembly_control_ros/supply_conveyor_output")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'supply_conveyor_output)))
  "Returns string type for a message object of type 'supply_conveyor_output"
  "assembly_control_ros/supply_conveyor_output")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<supply_conveyor_output>)))
  "Returns md5sum for a message object of type '<supply_conveyor_output>"
  "380de1277a09f4e060dd938224d3c515")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'supply_conveyor_output)))
  "Returns md5sum for a message object of type 'supply_conveyor_output"
  "380de1277a09f4e060dd938224d3c515")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<supply_conveyor_output>)))
  "Returns full string definition for message of type '<supply_conveyor_output>"
  (cl:format cl:nil "bool part_available~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'supply_conveyor_output)))
  "Returns full string definition for message of type 'supply_conveyor_output"
  (cl:format cl:nil "bool part_available~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <supply_conveyor_output>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <supply_conveyor_output>))
  "Converts a ROS message object to a list"
  (cl:list 'supply_conveyor_output
    (cl:cons ':part_available (part_available msg))
))
