; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude robot_state.msg.html

(cl:defclass <robot_state> (roslisp-msg-protocol:ros-message)
  ((part_grasped
    :reader part_grasped
    :initarg :part_grasped
    :type cl:boolean
    :initform cl:nil)
   (part_released
    :reader part_released
    :initarg :part_released
    :type cl:boolean
    :initform cl:nil)
   (at_supply_conveyor
    :reader at_supply_conveyor
    :initarg :at_supply_conveyor
    :type cl:boolean
    :initform cl:nil)
   (at_evacuation_conveyor
    :reader at_evacuation_conveyor
    :initarg :at_evacuation_conveyor
    :type cl:boolean
    :initform cl:nil)
   (at_assembly_station
    :reader at_assembly_station
    :initarg :at_assembly_station
    :type cl:boolean
    :initform cl:nil)
   (part1_assembled
    :reader part1_assembled
    :initarg :part1_assembled
    :type cl:boolean
    :initform cl:nil)
   (part2_assembled
    :reader part2_assembled
    :initarg :part2_assembled
    :type cl:boolean
    :initform cl:nil)
   (part3_assembled
    :reader part3_assembled
    :initarg :part3_assembled
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass robot_state (<robot_state>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <robot_state>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'robot_state)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<robot_state> is deprecated: use assembly_control_ros-msg:robot_state instead.")))

(cl:ensure-generic-function 'part_grasped-val :lambda-list '(m))
(cl:defmethod part_grasped-val ((m <robot_state>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:part_grasped-val is deprecated.  Use assembly_control_ros-msg:part_grasped instead.")
  (part_grasped m))

(cl:ensure-generic-function 'part_released-val :lambda-list '(m))
(cl:defmethod part_released-val ((m <robot_state>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:part_released-val is deprecated.  Use assembly_control_ros-msg:part_released instead.")
  (part_released m))

(cl:ensure-generic-function 'at_supply_conveyor-val :lambda-list '(m))
(cl:defmethod at_supply_conveyor-val ((m <robot_state>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:at_supply_conveyor-val is deprecated.  Use assembly_control_ros-msg:at_supply_conveyor instead.")
  (at_supply_conveyor m))

(cl:ensure-generic-function 'at_evacuation_conveyor-val :lambda-list '(m))
(cl:defmethod at_evacuation_conveyor-val ((m <robot_state>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:at_evacuation_conveyor-val is deprecated.  Use assembly_control_ros-msg:at_evacuation_conveyor instead.")
  (at_evacuation_conveyor m))

(cl:ensure-generic-function 'at_assembly_station-val :lambda-list '(m))
(cl:defmethod at_assembly_station-val ((m <robot_state>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:at_assembly_station-val is deprecated.  Use assembly_control_ros-msg:at_assembly_station instead.")
  (at_assembly_station m))

(cl:ensure-generic-function 'part1_assembled-val :lambda-list '(m))
(cl:defmethod part1_assembled-val ((m <robot_state>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:part1_assembled-val is deprecated.  Use assembly_control_ros-msg:part1_assembled instead.")
  (part1_assembled m))

(cl:ensure-generic-function 'part2_assembled-val :lambda-list '(m))
(cl:defmethod part2_assembled-val ((m <robot_state>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:part2_assembled-val is deprecated.  Use assembly_control_ros-msg:part2_assembled instead.")
  (part2_assembled m))

(cl:ensure-generic-function 'part3_assembled-val :lambda-list '(m))
(cl:defmethod part3_assembled-val ((m <robot_state>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:part3_assembled-val is deprecated.  Use assembly_control_ros-msg:part3_assembled instead.")
  (part3_assembled m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <robot_state>) ostream)
  "Serializes a message object of type '<robot_state>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'part_grasped) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'part_released) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'at_supply_conveyor) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'at_evacuation_conveyor) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'at_assembly_station) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'part1_assembled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'part2_assembled) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'part3_assembled) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <robot_state>) istream)
  "Deserializes a message object of type '<robot_state>"
    (cl:setf (cl:slot-value msg 'part_grasped) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'part_released) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'at_supply_conveyor) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'at_evacuation_conveyor) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'at_assembly_station) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'part1_assembled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'part2_assembled) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'part3_assembled) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<robot_state>)))
  "Returns string type for a message object of type '<robot_state>"
  "assembly_control_ros/robot_state")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'robot_state)))
  "Returns string type for a message object of type 'robot_state"
  "assembly_control_ros/robot_state")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<robot_state>)))
  "Returns md5sum for a message object of type '<robot_state>"
  "fc0a5945651ca653c6fce56198225a07")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'robot_state)))
  "Returns md5sum for a message object of type 'robot_state"
  "fc0a5945651ca653c6fce56198225a07")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<robot_state>)))
  "Returns full string definition for message of type '<robot_state>"
  (cl:format cl:nil "bool part_grasped~%bool part_released~%bool at_supply_conveyor~%bool at_evacuation_conveyor~%bool at_assembly_station~%bool part1_assembled~%bool part2_assembled~%bool part3_assembled~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'robot_state)))
  "Returns full string definition for message of type 'robot_state"
  (cl:format cl:nil "bool part_grasped~%bool part_released~%bool at_supply_conveyor~%bool at_evacuation_conveyor~%bool at_assembly_station~%bool part1_assembled~%bool part2_assembled~%bool part3_assembled~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <robot_state>))
  (cl:+ 0
     1
     1
     1
     1
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <robot_state>))
  "Converts a ROS message object to a list"
  (cl:list 'robot_state
    (cl:cons ':part_grasped (part_grasped msg))
    (cl:cons ':part_released (part_released msg))
    (cl:cons ':at_supply_conveyor (at_supply_conveyor msg))
    (cl:cons ':at_evacuation_conveyor (at_evacuation_conveyor msg))
    (cl:cons ':at_assembly_station (at_assembly_station msg))
    (cl:cons ':part1_assembled (part1_assembled msg))
    (cl:cons ':part2_assembled (part2_assembled msg))
    (cl:cons ':part3_assembled (part3_assembled msg))
))
