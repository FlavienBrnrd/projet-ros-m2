; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude evacuation_conveyor_input.msg.html

(cl:defclass <evacuation_conveyor_input> (roslisp-msg-protocol:ros-message)
  ((ROB_AT_EC
    :reader ROB_AT_EC
    :initarg :ROB_AT_EC
    :type cl:boolean
    :initform cl:nil)
   (ROB_RELEASED
    :reader ROB_RELEASED
    :initarg :ROB_RELEASED
    :type cl:boolean
    :initform cl:nil)
   (restart_EC
    :reader restart_EC
    :initarg :restart_EC
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass evacuation_conveyor_input (<evacuation_conveyor_input>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <evacuation_conveyor_input>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'evacuation_conveyor_input)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<evacuation_conveyor_input> is deprecated: use assembly_control_ros-msg:evacuation_conveyor_input instead.")))

(cl:ensure-generic-function 'ROB_AT_EC-val :lambda-list '(m))
(cl:defmethod ROB_AT_EC-val ((m <evacuation_conveyor_input>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:ROB_AT_EC-val is deprecated.  Use assembly_control_ros-msg:ROB_AT_EC instead.")
  (ROB_AT_EC m))

(cl:ensure-generic-function 'ROB_RELEASED-val :lambda-list '(m))
(cl:defmethod ROB_RELEASED-val ((m <evacuation_conveyor_input>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:ROB_RELEASED-val is deprecated.  Use assembly_control_ros-msg:ROB_RELEASED instead.")
  (ROB_RELEASED m))

(cl:ensure-generic-function 'restart_EC-val :lambda-list '(m))
(cl:defmethod restart_EC-val ((m <evacuation_conveyor_input>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:restart_EC-val is deprecated.  Use assembly_control_ros-msg:restart_EC instead.")
  (restart_EC m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <evacuation_conveyor_input>) ostream)
  "Serializes a message object of type '<evacuation_conveyor_input>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'ROB_AT_EC) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'ROB_RELEASED) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'restart_EC) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <evacuation_conveyor_input>) istream)
  "Deserializes a message object of type '<evacuation_conveyor_input>"
    (cl:setf (cl:slot-value msg 'ROB_AT_EC) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'ROB_RELEASED) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'restart_EC) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<evacuation_conveyor_input>)))
  "Returns string type for a message object of type '<evacuation_conveyor_input>"
  "assembly_control_ros/evacuation_conveyor_input")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'evacuation_conveyor_input)))
  "Returns string type for a message object of type 'evacuation_conveyor_input"
  "assembly_control_ros/evacuation_conveyor_input")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<evacuation_conveyor_input>)))
  "Returns md5sum for a message object of type '<evacuation_conveyor_input>"
  "f490813ad37f89aa0715c3042b565a3b")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'evacuation_conveyor_input)))
  "Returns md5sum for a message object of type 'evacuation_conveyor_input"
  "f490813ad37f89aa0715c3042b565a3b")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<evacuation_conveyor_input>)))
  "Returns full string definition for message of type '<evacuation_conveyor_input>"
  (cl:format cl:nil "bool ROB_AT_EC~%bool ROB_RELEASED~%bool restart_EC~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'evacuation_conveyor_input)))
  "Returns full string definition for message of type 'evacuation_conveyor_input"
  (cl:format cl:nil "bool ROB_AT_EC~%bool ROB_RELEASED~%bool restart_EC~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <evacuation_conveyor_input>))
  (cl:+ 0
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <evacuation_conveyor_input>))
  "Converts a ROS message object to a list"
  (cl:list 'evacuation_conveyor_input
    (cl:cons ':ROB_AT_EC (ROB_AT_EC msg))
    (cl:cons ':ROB_RELEASED (ROB_RELEASED msg))
    (cl:cons ':restart_EC (restart_EC msg))
))
