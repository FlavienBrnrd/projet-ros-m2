(cl:in-package assembly_control_ros-msg)
(cl:export '(GO_LEFT-VAL
          GO_LEFT
          GO_RIGHT-VAL
          GO_RIGHT
          GO_GRASP-VAL
          GO_GRASP
          GO_RELEASE-VAL
          GO_RELEASE
          GO_ASSEMBLE_1-VAL
          GO_ASSEMBLE_1
          GO_ASSEMBLE_2-VAL
          GO_ASSEMBLE_2
          GO_ASSEMBLE_3-VAL
          GO_ASSEMBLE_3
))