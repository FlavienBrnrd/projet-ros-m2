; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude robot_input.msg.html

(cl:defclass <robot_input> (roslisp-msg-protocol:ros-message)
  ((go_left
    :reader go_left
    :initarg :go_left
    :type cl:boolean
    :initform cl:nil)
   (go_right
    :reader go_right
    :initarg :go_right
    :type cl:boolean
    :initform cl:nil)
   (go_grasp
    :reader go_grasp
    :initarg :go_grasp
    :type cl:boolean
    :initform cl:nil)
   (go_release
    :reader go_release
    :initarg :go_release
    :type cl:boolean
    :initform cl:nil)
   (go_assemble_1
    :reader go_assemble_1
    :initarg :go_assemble_1
    :type cl:boolean
    :initform cl:nil)
   (go_assemble_2
    :reader go_assemble_2
    :initarg :go_assemble_2
    :type cl:boolean
    :initform cl:nil)
   (go_assemble_3
    :reader go_assemble_3
    :initarg :go_assemble_3
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass robot_input (<robot_input>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <robot_input>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'robot_input)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<robot_input> is deprecated: use assembly_control_ros-msg:robot_input instead.")))

(cl:ensure-generic-function 'go_left-val :lambda-list '(m))
(cl:defmethod go_left-val ((m <robot_input>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:go_left-val is deprecated.  Use assembly_control_ros-msg:go_left instead.")
  (go_left m))

(cl:ensure-generic-function 'go_right-val :lambda-list '(m))
(cl:defmethod go_right-val ((m <robot_input>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:go_right-val is deprecated.  Use assembly_control_ros-msg:go_right instead.")
  (go_right m))

(cl:ensure-generic-function 'go_grasp-val :lambda-list '(m))
(cl:defmethod go_grasp-val ((m <robot_input>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:go_grasp-val is deprecated.  Use assembly_control_ros-msg:go_grasp instead.")
  (go_grasp m))

(cl:ensure-generic-function 'go_release-val :lambda-list '(m))
(cl:defmethod go_release-val ((m <robot_input>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:go_release-val is deprecated.  Use assembly_control_ros-msg:go_release instead.")
  (go_release m))

(cl:ensure-generic-function 'go_assemble_1-val :lambda-list '(m))
(cl:defmethod go_assemble_1-val ((m <robot_input>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:go_assemble_1-val is deprecated.  Use assembly_control_ros-msg:go_assemble_1 instead.")
  (go_assemble_1 m))

(cl:ensure-generic-function 'go_assemble_2-val :lambda-list '(m))
(cl:defmethod go_assemble_2-val ((m <robot_input>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:go_assemble_2-val is deprecated.  Use assembly_control_ros-msg:go_assemble_2 instead.")
  (go_assemble_2 m))

(cl:ensure-generic-function 'go_assemble_3-val :lambda-list '(m))
(cl:defmethod go_assemble_3-val ((m <robot_input>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:go_assemble_3-val is deprecated.  Use assembly_control_ros-msg:go_assemble_3 instead.")
  (go_assemble_3 m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <robot_input>) ostream)
  "Serializes a message object of type '<robot_input>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'go_left) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'go_right) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'go_grasp) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'go_release) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'go_assemble_1) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'go_assemble_2) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'go_assemble_3) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <robot_input>) istream)
  "Deserializes a message object of type '<robot_input>"
    (cl:setf (cl:slot-value msg 'go_left) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'go_right) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'go_grasp) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'go_release) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'go_assemble_1) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'go_assemble_2) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'go_assemble_3) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<robot_input>)))
  "Returns string type for a message object of type '<robot_input>"
  "assembly_control_ros/robot_input")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'robot_input)))
  "Returns string type for a message object of type 'robot_input"
  "assembly_control_ros/robot_input")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<robot_input>)))
  "Returns md5sum for a message object of type '<robot_input>"
  "b449b320c3b2a2b17149f74a856e21ae")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'robot_input)))
  "Returns md5sum for a message object of type 'robot_input"
  "b449b320c3b2a2b17149f74a856e21ae")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<robot_input>)))
  "Returns full string definition for message of type '<robot_input>"
  (cl:format cl:nil "bool go_left~%bool go_right~%bool go_grasp~%bool go_release~%bool go_assemble_1~%bool go_assemble_2~%bool go_assemble_3~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'robot_input)))
  "Returns full string definition for message of type 'robot_input"
  (cl:format cl:nil "bool go_left~%bool go_right~%bool go_grasp~%bool go_release~%bool go_assemble_1~%bool go_assemble_2~%bool go_assemble_3~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <robot_input>))
  (cl:+ 0
     1
     1
     1
     1
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <robot_input>))
  "Converts a ROS message object to a list"
  (cl:list 'robot_input
    (cl:cons ':go_left (go_left msg))
    (cl:cons ':go_right (go_right msg))
    (cl:cons ':go_grasp (go_grasp msg))
    (cl:cons ':go_release (go_release msg))
    (cl:cons ':go_assemble_1 (go_assemble_1 msg))
    (cl:cons ':go_assemble_2 (go_assemble_2 msg))
    (cl:cons ':go_assemble_3 (go_assemble_3 msg))
))
