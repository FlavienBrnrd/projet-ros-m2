; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude assembly_station_output.msg.html

(cl:defclass <assembly_station_output> (roslisp-msg-protocol:ros-message)
  ((AS_VALID
    :reader AS_VALID
    :initarg :AS_VALID
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass assembly_station_output (<assembly_station_output>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <assembly_station_output>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'assembly_station_output)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<assembly_station_output> is deprecated: use assembly_control_ros-msg:assembly_station_output instead.")))

(cl:ensure-generic-function 'AS_VALID-val :lambda-list '(m))
(cl:defmethod AS_VALID-val ((m <assembly_station_output>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:AS_VALID-val is deprecated.  Use assembly_control_ros-msg:AS_VALID instead.")
  (AS_VALID m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <assembly_station_output>) ostream)
  "Serializes a message object of type '<assembly_station_output>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'AS_VALID) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <assembly_station_output>) istream)
  "Deserializes a message object of type '<assembly_station_output>"
    (cl:setf (cl:slot-value msg 'AS_VALID) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<assembly_station_output>)))
  "Returns string type for a message object of type '<assembly_station_output>"
  "assembly_control_ros/assembly_station_output")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'assembly_station_output)))
  "Returns string type for a message object of type 'assembly_station_output"
  "assembly_control_ros/assembly_station_output")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<assembly_station_output>)))
  "Returns md5sum for a message object of type '<assembly_station_output>"
  "dc50fef128a1d9b4a668a05575aa5293")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'assembly_station_output)))
  "Returns md5sum for a message object of type 'assembly_station_output"
  "dc50fef128a1d9b4a668a05575aa5293")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<assembly_station_output>)))
  "Returns full string definition for message of type '<assembly_station_output>"
  (cl:format cl:nil "bool AS_VALID~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'assembly_station_output)))
  "Returns full string definition for message of type 'assembly_station_output"
  (cl:format cl:nil "bool AS_VALID~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <assembly_station_output>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <assembly_station_output>))
  "Converts a ROS message object to a list"
  (cl:list 'assembly_station_output
    (cl:cons ':AS_VALID (AS_VALID msg))
))
