; Auto-generated. Do not edit!


(cl:in-package assembly_control_ros-msg)


;//! \htmlinclude evacuation_conveyor_command.msg.html

(cl:defclass <evacuation_conveyor_command> (roslisp-msg-protocol:ros-message)
  ((on
    :reader on
    :initarg :on
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass evacuation_conveyor_command (<evacuation_conveyor_command>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <evacuation_conveyor_command>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'evacuation_conveyor_command)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name assembly_control_ros-msg:<evacuation_conveyor_command> is deprecated: use assembly_control_ros-msg:evacuation_conveyor_command instead.")))

(cl:ensure-generic-function 'on-val :lambda-list '(m))
(cl:defmethod on-val ((m <evacuation_conveyor_command>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader assembly_control_ros-msg:on-val is deprecated.  Use assembly_control_ros-msg:on instead.")
  (on m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <evacuation_conveyor_command>) ostream)
  "Serializes a message object of type '<evacuation_conveyor_command>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'on) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <evacuation_conveyor_command>) istream)
  "Deserializes a message object of type '<evacuation_conveyor_command>"
    (cl:setf (cl:slot-value msg 'on) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<evacuation_conveyor_command>)))
  "Returns string type for a message object of type '<evacuation_conveyor_command>"
  "assembly_control_ros/evacuation_conveyor_command")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'evacuation_conveyor_command)))
  "Returns string type for a message object of type 'evacuation_conveyor_command"
  "assembly_control_ros/evacuation_conveyor_command")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<evacuation_conveyor_command>)))
  "Returns md5sum for a message object of type '<evacuation_conveyor_command>"
  "74983d2ffe4877de8ae30b7a94625c41")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'evacuation_conveyor_command)))
  "Returns md5sum for a message object of type 'evacuation_conveyor_command"
  "74983d2ffe4877de8ae30b7a94625c41")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<evacuation_conveyor_command>)))
  "Returns full string definition for message of type '<evacuation_conveyor_command>"
  (cl:format cl:nil "bool on~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'evacuation_conveyor_command)))
  "Returns full string definition for message of type 'evacuation_conveyor_command"
  (cl:format cl:nil "bool on~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <evacuation_conveyor_command>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <evacuation_conveyor_command>))
  "Converts a ROS message object to a list"
  (cl:list 'evacuation_conveyor_command
    (cl:cons ':on (on msg))
))
