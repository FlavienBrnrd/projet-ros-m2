// Auto-generated. Do not edit!

// (in-package assembly_control_ros.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class supply_conveyor_state {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.optical_barrier = null;
    }
    else {
      if (initObj.hasOwnProperty('optical_barrier')) {
        this.optical_barrier = initObj.optical_barrier
      }
      else {
        this.optical_barrier = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type supply_conveyor_state
    // Serialize message field [optical_barrier]
    bufferOffset = _serializer.bool(obj.optical_barrier, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type supply_conveyor_state
    let len;
    let data = new supply_conveyor_state(null);
    // Deserialize message field [optical_barrier]
    data.optical_barrier = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a message object
    return 'assembly_control_ros/supply_conveyor_state';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '36bbab7385881955fe117c4d8550a944';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool optical_barrier
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new supply_conveyor_state(null);
    if (msg.optical_barrier !== undefined) {
      resolved.optical_barrier = msg.optical_barrier;
    }
    else {
      resolved.optical_barrier = false
    }

    return resolved;
    }
};

module.exports = supply_conveyor_state;
