// Auto-generated. Do not edit!

// (in-package assembly_control_ros.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class evacuation_conveyor_input {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.ROB_AT_EC = null;
      this.ROB_RELEASED = null;
      this.restart_EC = null;
    }
    else {
      if (initObj.hasOwnProperty('ROB_AT_EC')) {
        this.ROB_AT_EC = initObj.ROB_AT_EC
      }
      else {
        this.ROB_AT_EC = false;
      }
      if (initObj.hasOwnProperty('ROB_RELEASED')) {
        this.ROB_RELEASED = initObj.ROB_RELEASED
      }
      else {
        this.ROB_RELEASED = false;
      }
      if (initObj.hasOwnProperty('restart_EC')) {
        this.restart_EC = initObj.restart_EC
      }
      else {
        this.restart_EC = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type evacuation_conveyor_input
    // Serialize message field [ROB_AT_EC]
    bufferOffset = _serializer.bool(obj.ROB_AT_EC, buffer, bufferOffset);
    // Serialize message field [ROB_RELEASED]
    bufferOffset = _serializer.bool(obj.ROB_RELEASED, buffer, bufferOffset);
    // Serialize message field [restart_EC]
    bufferOffset = _serializer.bool(obj.restart_EC, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type evacuation_conveyor_input
    let len;
    let data = new evacuation_conveyor_input(null);
    // Deserialize message field [ROB_AT_EC]
    data.ROB_AT_EC = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [ROB_RELEASED]
    data.ROB_RELEASED = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [restart_EC]
    data.restart_EC = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 3;
  }

  static datatype() {
    // Returns string type for a message object
    return 'assembly_control_ros/evacuation_conveyor_input';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'f490813ad37f89aa0715c3042b565a3b';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool ROB_AT_EC
    bool ROB_RELEASED
    bool restart_EC
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new evacuation_conveyor_input(null);
    if (msg.ROB_AT_EC !== undefined) {
      resolved.ROB_AT_EC = msg.ROB_AT_EC;
    }
    else {
      resolved.ROB_AT_EC = false
    }

    if (msg.ROB_RELEASED !== undefined) {
      resolved.ROB_RELEASED = msg.ROB_RELEASED;
    }
    else {
      resolved.ROB_RELEASED = false
    }

    if (msg.restart_EC !== undefined) {
      resolved.restart_EC = msg.restart_EC;
    }
    else {
      resolved.restart_EC = false
    }

    return resolved;
    }
};

module.exports = evacuation_conveyor_input;
