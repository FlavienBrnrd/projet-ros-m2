// Auto-generated. Do not edit!

// (in-package assembly_control_ros.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class supply_conveyor_output {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.part_available = null;
    }
    else {
      if (initObj.hasOwnProperty('part_available')) {
        this.part_available = initObj.part_available
      }
      else {
        this.part_available = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type supply_conveyor_output
    // Serialize message field [part_available]
    bufferOffset = _serializer.bool(obj.part_available, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type supply_conveyor_output
    let len;
    let data = new supply_conveyor_output(null);
    // Deserialize message field [part_available]
    data.part_available = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a message object
    return 'assembly_control_ros/supply_conveyor_output';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '380de1277a09f4e060dd938224d3c515';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool part_available
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new supply_conveyor_output(null);
    if (msg.part_available !== undefined) {
      resolved.part_available = msg.part_available;
    }
    else {
      resolved.part_available = false
    }

    return resolved;
    }
};

module.exports = supply_conveyor_output;
