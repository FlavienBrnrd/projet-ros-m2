// Auto-generated. Do not edit!

// (in-package assembly_control_ros.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class assembly_station_input {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.PARTS_DONE = null;
    }
    else {
      if (initObj.hasOwnProperty('PARTS_DONE')) {
        this.PARTS_DONE = initObj.PARTS_DONE
      }
      else {
        this.PARTS_DONE = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type assembly_station_input
    // Serialize message field [PARTS_DONE]
    bufferOffset = _serializer.bool(obj.PARTS_DONE, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type assembly_station_input
    let len;
    let data = new assembly_station_input(null);
    // Deserialize message field [PARTS_DONE]
    data.PARTS_DONE = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a message object
    return 'assembly_control_ros/assembly_station_input';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'f6176f35c766c927d57bbd15eeee6d4b';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    
    bool PARTS_DONE
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new assembly_station_input(null);
    if (msg.PARTS_DONE !== undefined) {
      resolved.PARTS_DONE = msg.PARTS_DONE;
    }
    else {
      resolved.PARTS_DONE = false
    }

    return resolved;
    }
};

module.exports = assembly_station_input;
