// Auto-generated. Do not edit!

// (in-package assembly_control_ros.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class camera_output {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.part_analyzed = null;
      this.part1 = null;
      this.part2 = null;
      this.part3 = null;
    }
    else {
      if (initObj.hasOwnProperty('part_analyzed')) {
        this.part_analyzed = initObj.part_analyzed
      }
      else {
        this.part_analyzed = false;
      }
      if (initObj.hasOwnProperty('part1')) {
        this.part1 = initObj.part1
      }
      else {
        this.part1 = false;
      }
      if (initObj.hasOwnProperty('part2')) {
        this.part2 = initObj.part2
      }
      else {
        this.part2 = false;
      }
      if (initObj.hasOwnProperty('part3')) {
        this.part3 = initObj.part3
      }
      else {
        this.part3 = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type camera_output
    // Serialize message field [part_analyzed]
    bufferOffset = _serializer.bool(obj.part_analyzed, buffer, bufferOffset);
    // Serialize message field [part1]
    bufferOffset = _serializer.bool(obj.part1, buffer, bufferOffset);
    // Serialize message field [part2]
    bufferOffset = _serializer.bool(obj.part2, buffer, bufferOffset);
    // Serialize message field [part3]
    bufferOffset = _serializer.bool(obj.part3, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type camera_output
    let len;
    let data = new camera_output(null);
    // Deserialize message field [part_analyzed]
    data.part_analyzed = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [part1]
    data.part1 = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [part2]
    data.part2 = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [part3]
    data.part3 = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 4;
  }

  static datatype() {
    // Returns string type for a message object
    return 'assembly_control_ros/camera_output';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'f03ae215b98c387da3ca20f4eef0f6bf';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool part_analyzed
    bool part1
    bool part2
    bool part3
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new camera_output(null);
    if (msg.part_analyzed !== undefined) {
      resolved.part_analyzed = msg.part_analyzed;
    }
    else {
      resolved.part_analyzed = false
    }

    if (msg.part1 !== undefined) {
      resolved.part1 = msg.part1;
    }
    else {
      resolved.part1 = false
    }

    if (msg.part2 !== undefined) {
      resolved.part2 = msg.part2;
    }
    else {
      resolved.part2 = false
    }

    if (msg.part3 !== undefined) {
      resolved.part3 = msg.part3;
    }
    else {
      resolved.part3 = false
    }

    return resolved;
    }
};

module.exports = camera_output;
