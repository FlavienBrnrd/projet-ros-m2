// Auto-generated. Do not edit!

// (in-package assembly_control_ros.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class robot_input {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.go_left = null;
      this.go_right = null;
      this.go_grasp = null;
      this.go_release = null;
      this.go_assemble_1 = null;
      this.go_assemble_2 = null;
      this.go_assemble_3 = null;
    }
    else {
      if (initObj.hasOwnProperty('go_left')) {
        this.go_left = initObj.go_left
      }
      else {
        this.go_left = false;
      }
      if (initObj.hasOwnProperty('go_right')) {
        this.go_right = initObj.go_right
      }
      else {
        this.go_right = false;
      }
      if (initObj.hasOwnProperty('go_grasp')) {
        this.go_grasp = initObj.go_grasp
      }
      else {
        this.go_grasp = false;
      }
      if (initObj.hasOwnProperty('go_release')) {
        this.go_release = initObj.go_release
      }
      else {
        this.go_release = false;
      }
      if (initObj.hasOwnProperty('go_assemble_1')) {
        this.go_assemble_1 = initObj.go_assemble_1
      }
      else {
        this.go_assemble_1 = false;
      }
      if (initObj.hasOwnProperty('go_assemble_2')) {
        this.go_assemble_2 = initObj.go_assemble_2
      }
      else {
        this.go_assemble_2 = false;
      }
      if (initObj.hasOwnProperty('go_assemble_3')) {
        this.go_assemble_3 = initObj.go_assemble_3
      }
      else {
        this.go_assemble_3 = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type robot_input
    // Serialize message field [go_left]
    bufferOffset = _serializer.bool(obj.go_left, buffer, bufferOffset);
    // Serialize message field [go_right]
    bufferOffset = _serializer.bool(obj.go_right, buffer, bufferOffset);
    // Serialize message field [go_grasp]
    bufferOffset = _serializer.bool(obj.go_grasp, buffer, bufferOffset);
    // Serialize message field [go_release]
    bufferOffset = _serializer.bool(obj.go_release, buffer, bufferOffset);
    // Serialize message field [go_assemble_1]
    bufferOffset = _serializer.bool(obj.go_assemble_1, buffer, bufferOffset);
    // Serialize message field [go_assemble_2]
    bufferOffset = _serializer.bool(obj.go_assemble_2, buffer, bufferOffset);
    // Serialize message field [go_assemble_3]
    bufferOffset = _serializer.bool(obj.go_assemble_3, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type robot_input
    let len;
    let data = new robot_input(null);
    // Deserialize message field [go_left]
    data.go_left = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [go_right]
    data.go_right = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [go_grasp]
    data.go_grasp = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [go_release]
    data.go_release = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [go_assemble_1]
    data.go_assemble_1 = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [go_assemble_2]
    data.go_assemble_2 = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [go_assemble_3]
    data.go_assemble_3 = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 7;
  }

  static datatype() {
    // Returns string type for a message object
    return 'assembly_control_ros/robot_input';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'b449b320c3b2a2b17149f74a856e21ae';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool go_left
    bool go_right
    bool go_grasp
    bool go_release
    bool go_assemble_1
    bool go_assemble_2
    bool go_assemble_3
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new robot_input(null);
    if (msg.go_left !== undefined) {
      resolved.go_left = msg.go_left;
    }
    else {
      resolved.go_left = false
    }

    if (msg.go_right !== undefined) {
      resolved.go_right = msg.go_right;
    }
    else {
      resolved.go_right = false
    }

    if (msg.go_grasp !== undefined) {
      resolved.go_grasp = msg.go_grasp;
    }
    else {
      resolved.go_grasp = false
    }

    if (msg.go_release !== undefined) {
      resolved.go_release = msg.go_release;
    }
    else {
      resolved.go_release = false
    }

    if (msg.go_assemble_1 !== undefined) {
      resolved.go_assemble_1 = msg.go_assemble_1;
    }
    else {
      resolved.go_assemble_1 = false
    }

    if (msg.go_assemble_2 !== undefined) {
      resolved.go_assemble_2 = msg.go_assemble_2;
    }
    else {
      resolved.go_assemble_2 = false
    }

    if (msg.go_assemble_3 !== undefined) {
      resolved.go_assemble_3 = msg.go_assemble_3;
    }
    else {
      resolved.go_assemble_3 = false
    }

    return resolved;
    }
};

module.exports = robot_input;
