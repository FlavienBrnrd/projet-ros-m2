// Auto-generated. Do not edit!

// (in-package assembly_control_ros.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class camera_command {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.process = null;
    }
    else {
      if (initObj.hasOwnProperty('process')) {
        this.process = initObj.process
      }
      else {
        this.process = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type camera_command
    // Serialize message field [process]
    bufferOffset = _serializer.bool(obj.process, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type camera_command
    let len;
    let data = new camera_command(null);
    // Deserialize message field [process]
    data.process = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a message object
    return 'assembly_control_ros/camera_command';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '1c650d1d3c30589cc2658a353420cbf2';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool process
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new camera_command(null);
    if (msg.process !== undefined) {
      resolved.process = msg.process;
    }
    else {
      resolved.process = false
    }

    return resolved;
    }
};

module.exports = camera_command;
