// Auto-generated. Do not edit!

// (in-package assembly_control_ros.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class evacuation_conveyor_output {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.EC_STOPPED = null;
    }
    else {
      if (initObj.hasOwnProperty('EC_STOPPED')) {
        this.EC_STOPPED = initObj.EC_STOPPED
      }
      else {
        this.EC_STOPPED = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type evacuation_conveyor_output
    // Serialize message field [EC_STOPPED]
    bufferOffset = _serializer.bool(obj.EC_STOPPED, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type evacuation_conveyor_output
    let len;
    let data = new evacuation_conveyor_output(null);
    // Deserialize message field [EC_STOPPED]
    data.EC_STOPPED = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a message object
    return 'assembly_control_ros/evacuation_conveyor_output';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'c09c96dee6aef70265cad5be94ee4a4d';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool EC_STOPPED
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new evacuation_conveyor_output(null);
    if (msg.EC_STOPPED !== undefined) {
      resolved.EC_STOPPED = msg.EC_STOPPED;
    }
    else {
      resolved.EC_STOPPED = false
    }

    return resolved;
    }
};

module.exports = evacuation_conveyor_output;
