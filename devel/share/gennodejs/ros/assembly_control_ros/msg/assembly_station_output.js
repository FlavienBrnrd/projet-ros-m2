// Auto-generated. Do not edit!

// (in-package assembly_control_ros.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class assembly_station_output {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.AS_VALID = null;
    }
    else {
      if (initObj.hasOwnProperty('AS_VALID')) {
        this.AS_VALID = initObj.AS_VALID
      }
      else {
        this.AS_VALID = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type assembly_station_output
    // Serialize message field [AS_VALID]
    bufferOffset = _serializer.bool(obj.AS_VALID, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type assembly_station_output
    let len;
    let data = new assembly_station_output(null);
    // Deserialize message field [AS_VALID]
    data.AS_VALID = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a message object
    return 'assembly_control_ros/assembly_station_output';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'dc50fef128a1d9b4a668a05575aa5293';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool AS_VALID
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new assembly_station_output(null);
    if (msg.AS_VALID !== undefined) {
      resolved.AS_VALID = msg.AS_VALID;
    }
    else {
      resolved.AS_VALID = false
    }

    return resolved;
    }
};

module.exports = assembly_station_output;
