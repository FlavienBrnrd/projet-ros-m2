// Auto-generated. Do not edit!

// (in-package assembly_control_ros.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class robot_command {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.move_right = null;
      this.move_left = null;
      this.grasp = null;
      this.release = null;
      this.assemble_part1 = null;
      this.assemble_part2 = null;
      this.assemble_part3 = null;
    }
    else {
      if (initObj.hasOwnProperty('move_right')) {
        this.move_right = initObj.move_right
      }
      else {
        this.move_right = false;
      }
      if (initObj.hasOwnProperty('move_left')) {
        this.move_left = initObj.move_left
      }
      else {
        this.move_left = false;
      }
      if (initObj.hasOwnProperty('grasp')) {
        this.grasp = initObj.grasp
      }
      else {
        this.grasp = false;
      }
      if (initObj.hasOwnProperty('release')) {
        this.release = initObj.release
      }
      else {
        this.release = false;
      }
      if (initObj.hasOwnProperty('assemble_part1')) {
        this.assemble_part1 = initObj.assemble_part1
      }
      else {
        this.assemble_part1 = false;
      }
      if (initObj.hasOwnProperty('assemble_part2')) {
        this.assemble_part2 = initObj.assemble_part2
      }
      else {
        this.assemble_part2 = false;
      }
      if (initObj.hasOwnProperty('assemble_part3')) {
        this.assemble_part3 = initObj.assemble_part3
      }
      else {
        this.assemble_part3 = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type robot_command
    // Serialize message field [move_right]
    bufferOffset = _serializer.bool(obj.move_right, buffer, bufferOffset);
    // Serialize message field [move_left]
    bufferOffset = _serializer.bool(obj.move_left, buffer, bufferOffset);
    // Serialize message field [grasp]
    bufferOffset = _serializer.bool(obj.grasp, buffer, bufferOffset);
    // Serialize message field [release]
    bufferOffset = _serializer.bool(obj.release, buffer, bufferOffset);
    // Serialize message field [assemble_part1]
    bufferOffset = _serializer.bool(obj.assemble_part1, buffer, bufferOffset);
    // Serialize message field [assemble_part2]
    bufferOffset = _serializer.bool(obj.assemble_part2, buffer, bufferOffset);
    // Serialize message field [assemble_part3]
    bufferOffset = _serializer.bool(obj.assemble_part3, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type robot_command
    let len;
    let data = new robot_command(null);
    // Deserialize message field [move_right]
    data.move_right = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [move_left]
    data.move_left = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [grasp]
    data.grasp = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [release]
    data.release = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [assemble_part1]
    data.assemble_part1 = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [assemble_part2]
    data.assemble_part2 = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [assemble_part3]
    data.assemble_part3 = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 7;
  }

  static datatype() {
    // Returns string type for a message object
    return 'assembly_control_ros/robot_command';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '90dd75667caf471745cdc1c8deb81f71';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool move_right
    bool move_left
    bool grasp
    bool release
    bool assemble_part1
    bool assemble_part2
    bool assemble_part3
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new robot_command(null);
    if (msg.move_right !== undefined) {
      resolved.move_right = msg.move_right;
    }
    else {
      resolved.move_right = false
    }

    if (msg.move_left !== undefined) {
      resolved.move_left = msg.move_left;
    }
    else {
      resolved.move_left = false
    }

    if (msg.grasp !== undefined) {
      resolved.grasp = msg.grasp;
    }
    else {
      resolved.grasp = false
    }

    if (msg.release !== undefined) {
      resolved.release = msg.release;
    }
    else {
      resolved.release = false
    }

    if (msg.assemble_part1 !== undefined) {
      resolved.assemble_part1 = msg.assemble_part1;
    }
    else {
      resolved.assemble_part1 = false
    }

    if (msg.assemble_part2 !== undefined) {
      resolved.assemble_part2 = msg.assemble_part2;
    }
    else {
      resolved.assemble_part2 = false
    }

    if (msg.assemble_part3 !== undefined) {
      resolved.assemble_part3 = msg.assemble_part3;
    }
    else {
      resolved.assemble_part3 = false
    }

    return resolved;
    }
};

module.exports = robot_command;
