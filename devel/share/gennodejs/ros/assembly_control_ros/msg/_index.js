
"use strict";

let camera_state = require('./camera_state.js');
let robot_command = require('./robot_command.js');
let supply_conveyor_state = require('./supply_conveyor_state.js');
let evacuation_conveyor_input = require('./evacuation_conveyor_input.js');
let camera_output = require('./camera_output.js');
let assembly_station_input = require('./assembly_station_input.js');
let robot_input = require('./robot_input.js');
let supply_conveyor_input = require('./supply_conveyor_input.js');
let assembly_station_command = require('./assembly_station_command.js');
let robot_output = require('./robot_output.js');
let assembly_station_output = require('./assembly_station_output.js');
let supply_conveyor_output = require('./supply_conveyor_output.js');
let camera_command = require('./camera_command.js');
let evacuation_conveyor_output = require('./evacuation_conveyor_output.js');
let evacuation_conveyor_command = require('./evacuation_conveyor_command.js');
let assembly_station_state = require('./assembly_station_state.js');
let robot_state = require('./robot_state.js');
let supply_conveyor_command = require('./supply_conveyor_command.js');
let evacuation_conveyor_state = require('./evacuation_conveyor_state.js');
let camera_input = require('./camera_input.js');

module.exports = {
  camera_state: camera_state,
  robot_command: robot_command,
  supply_conveyor_state: supply_conveyor_state,
  evacuation_conveyor_input: evacuation_conveyor_input,
  camera_output: camera_output,
  assembly_station_input: assembly_station_input,
  robot_input: robot_input,
  supply_conveyor_input: supply_conveyor_input,
  assembly_station_command: assembly_station_command,
  robot_output: robot_output,
  assembly_station_output: assembly_station_output,
  supply_conveyor_output: supply_conveyor_output,
  camera_command: camera_command,
  evacuation_conveyor_output: evacuation_conveyor_output,
  evacuation_conveyor_command: evacuation_conveyor_command,
  assembly_station_state: assembly_station_state,
  robot_state: robot_state,
  supply_conveyor_command: supply_conveyor_command,
  evacuation_conveyor_state: evacuation_conveyor_state,
  camera_input: camera_input,
};
