// Auto-generated. Do not edit!

// (in-package assembly_control_ros.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class robot_output {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.go_left_done = null;
      this.go_right_done = null;
      this.go_grasp_done = null;
      this.go_release_done = null;
      this.assemble_1_done = null;
      this.assemble_2_done = null;
      this.assemble_3_done = null;
    }
    else {
      if (initObj.hasOwnProperty('go_left_done')) {
        this.go_left_done = initObj.go_left_done
      }
      else {
        this.go_left_done = false;
      }
      if (initObj.hasOwnProperty('go_right_done')) {
        this.go_right_done = initObj.go_right_done
      }
      else {
        this.go_right_done = false;
      }
      if (initObj.hasOwnProperty('go_grasp_done')) {
        this.go_grasp_done = initObj.go_grasp_done
      }
      else {
        this.go_grasp_done = false;
      }
      if (initObj.hasOwnProperty('go_release_done')) {
        this.go_release_done = initObj.go_release_done
      }
      else {
        this.go_release_done = false;
      }
      if (initObj.hasOwnProperty('assemble_1_done')) {
        this.assemble_1_done = initObj.assemble_1_done
      }
      else {
        this.assemble_1_done = false;
      }
      if (initObj.hasOwnProperty('assemble_2_done')) {
        this.assemble_2_done = initObj.assemble_2_done
      }
      else {
        this.assemble_2_done = false;
      }
      if (initObj.hasOwnProperty('assemble_3_done')) {
        this.assemble_3_done = initObj.assemble_3_done
      }
      else {
        this.assemble_3_done = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type robot_output
    // Serialize message field [go_left_done]
    bufferOffset = _serializer.bool(obj.go_left_done, buffer, bufferOffset);
    // Serialize message field [go_right_done]
    bufferOffset = _serializer.bool(obj.go_right_done, buffer, bufferOffset);
    // Serialize message field [go_grasp_done]
    bufferOffset = _serializer.bool(obj.go_grasp_done, buffer, bufferOffset);
    // Serialize message field [go_release_done]
    bufferOffset = _serializer.bool(obj.go_release_done, buffer, bufferOffset);
    // Serialize message field [assemble_1_done]
    bufferOffset = _serializer.bool(obj.assemble_1_done, buffer, bufferOffset);
    // Serialize message field [assemble_2_done]
    bufferOffset = _serializer.bool(obj.assemble_2_done, buffer, bufferOffset);
    // Serialize message field [assemble_3_done]
    bufferOffset = _serializer.bool(obj.assemble_3_done, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type robot_output
    let len;
    let data = new robot_output(null);
    // Deserialize message field [go_left_done]
    data.go_left_done = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [go_right_done]
    data.go_right_done = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [go_grasp_done]
    data.go_grasp_done = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [go_release_done]
    data.go_release_done = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [assemble_1_done]
    data.assemble_1_done = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [assemble_2_done]
    data.assemble_2_done = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [assemble_3_done]
    data.assemble_3_done = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 7;
  }

  static datatype() {
    // Returns string type for a message object
    return 'assembly_control_ros/robot_output';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'f860ddfdf175592b078c56f3a3fb1cee';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool go_left_done
    bool go_right_done
    bool go_grasp_done
    bool go_release_done
    bool assemble_1_done
    bool assemble_2_done
    bool assemble_3_done
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new robot_output(null);
    if (msg.go_left_done !== undefined) {
      resolved.go_left_done = msg.go_left_done;
    }
    else {
      resolved.go_left_done = false
    }

    if (msg.go_right_done !== undefined) {
      resolved.go_right_done = msg.go_right_done;
    }
    else {
      resolved.go_right_done = false
    }

    if (msg.go_grasp_done !== undefined) {
      resolved.go_grasp_done = msg.go_grasp_done;
    }
    else {
      resolved.go_grasp_done = false
    }

    if (msg.go_release_done !== undefined) {
      resolved.go_release_done = msg.go_release_done;
    }
    else {
      resolved.go_release_done = false
    }

    if (msg.assemble_1_done !== undefined) {
      resolved.assemble_1_done = msg.assemble_1_done;
    }
    else {
      resolved.assemble_1_done = false
    }

    if (msg.assemble_2_done !== undefined) {
      resolved.assemble_2_done = msg.assemble_2_done;
    }
    else {
      resolved.assemble_2_done = false
    }

    if (msg.assemble_3_done !== undefined) {
      resolved.assemble_3_done = msg.assemble_3_done;
    }
    else {
      resolved.assemble_3_done = false
    }

    return resolved;
    }
};

module.exports = robot_output;
