# CMake generated Testfile for 
# Source directory: /home/flavien/catkin_ws/src
# Build directory: /home/flavien/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("firebot")
subdirs("firebot_control")
subdirs("firebotv2")
