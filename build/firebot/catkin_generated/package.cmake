set(_CATKIN_CURRENT_PACKAGE "firebot")
set(firebot_VERSION "0.0.0")
set(firebot_MAINTAINER "flavien <flavien@todo.todo>")
set(firebot_PACKAGE_FORMAT "2")
set(firebot_BUILD_DEPENDS "roscpp" "std_msgs")
set(firebot_BUILD_EXPORT_DEPENDS "roscpp" "std_msgs")
set(firebot_BUILDTOOL_DEPENDS "catkin")
set(firebot_BUILDTOOL_EXPORT_DEPENDS )
set(firebot_EXEC_DEPENDS "roscpp" "std_msgs")
set(firebot_RUN_DEPENDS "roscpp" "std_msgs")
set(firebot_TEST_DEPENDS )
set(firebot_DOC_DEPENDS )
set(firebot_URL_WEBSITE "")
set(firebot_URL_BUGTRACKER "")
set(firebot_URL_REPOSITORY "")
set(firebot_DEPRECATED "")