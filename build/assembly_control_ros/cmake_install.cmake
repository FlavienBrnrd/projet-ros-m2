# Install script for directory: /home/flavien/catkin_ws/src/assembly_control_ros

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/flavien/catkin_ws/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/assembly_control_ros/msg" TYPE FILE FILES
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/assembly_station_command.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/assembly_station_input.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/assembly_station_output.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/assembly_station_state.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/camera_command.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/camera_input.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/camera_output.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/camera_state.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/evacuation_conveyor_command.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/evacuation_conveyor_input.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/evacuation_conveyor_output.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/evacuation_conveyor_state.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/robot_command.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/robot_input.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/robot_output.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/robot_state.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/supply_conveyor_command.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/supply_conveyor_input.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/supply_conveyor_output.msg"
    "/home/flavien/catkin_ws/src/assembly_control_ros/msg/supply_conveyor_state.msg"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/assembly_control_ros/cmake" TYPE FILE FILES "/home/flavien/catkin_ws/build/assembly_control_ros/catkin_generated/installspace/assembly_control_ros-msg-paths.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/home/flavien/catkin_ws/devel/include/assembly_control_ros")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/roseus/ros" TYPE DIRECTORY FILES "/home/flavien/catkin_ws/devel/share/roseus/ros/assembly_control_ros")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common-lisp/ros" TYPE DIRECTORY FILES "/home/flavien/catkin_ws/devel/share/common-lisp/ros/assembly_control_ros")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gennodejs/ros" TYPE DIRECTORY FILES "/home/flavien/catkin_ws/devel/share/gennodejs/ros/assembly_control_ros")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  execute_process(COMMAND "/usr/bin/python2" -m compileall "/home/flavien/catkin_ws/devel/lib/python2.7/dist-packages/assembly_control_ros")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python2.7/dist-packages" TYPE DIRECTORY FILES "/home/flavien/catkin_ws/devel/lib/python2.7/dist-packages/assembly_control_ros")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/flavien/catkin_ws/build/assembly_control_ros/catkin_generated/installspace/assembly_control_ros.pc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/assembly_control_ros/cmake" TYPE FILE FILES "/home/flavien/catkin_ws/build/assembly_control_ros/catkin_generated/installspace/assembly_control_ros-msg-extras.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/assembly_control_ros/cmake" TYPE FILE FILES
    "/home/flavien/catkin_ws/build/assembly_control_ros/catkin_generated/installspace/assembly_control_rosConfig.cmake"
    "/home/flavien/catkin_ws/build/assembly_control_ros/catkin_generated/installspace/assembly_control_rosConfig-version.cmake"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/assembly_control_ros" TYPE FILE FILES "/home/flavien/catkin_ws/src/assembly_control_ros/package.xml")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/flavien/catkin_ws/build/assembly_control_ros/src/cmake_install.cmake")

endif()

