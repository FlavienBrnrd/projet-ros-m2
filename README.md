**Semaine du 9 au 13 septembre 2019**

Creation du robot mobile le FireBot ==> objectif : le modeliser, puis le faire bouger

*prise en main de Ros, tutoriels de Ros, étude des package et node*

**Semaine du 16 au  20 septembre 2019**

*Creation du dépôt gitlab creation du Workspace pour ros*

**vendredi 20 septembre 2019**

* Correction d'un bug à cause duquel le model URDF ne pouvait pas être afficher sous Rviz
* Ajout des Roues (folle, droite et gauche) ainsi que les liens avec les chassis
* Generation avec un urdf tools du graphnqui represente mon fichier urdf voir log

```
robot name is: firebot
---------- Successfully Parsed XML ---------------
root Link: base has 3 child(ren)
    child(1):  chassis
    child(2):  roue_droite
    child(3):  roue_gauche
```
**Semaine du 23 au  27 septembre 2019**

**mercredi 25 septembre 2019**

* Correction d'un bug à cause duquel le model URDF ne pouvait pas être afficher CORRECTEMENT sous gazebo
* Ajout du fichier final du "visuel" du firebot
* Travail sur le fichier firebot5.gazebo pour incorporer gazebo_ros_control (package pour faire bouger)

**vendredi 27 sptembre 2019**

*Tutoriel sur la transmission et les effecteurs dans Gazebo*

1. (http://gazebosim.org/tutorials/?tut=ros_control)
2. (http://gazebosim.org/tutorials?tut=ros_gzplugins)

création d'un nouveau package ==> firebot_control avec 3 plugins 
`(catkin_create_pkg firebot_control controller_manager joint_state_controller robot_state_publisher)`

Test 1 :
* Creation d'un fichier de configutation .yaml pour le firebot_control
* Creation d'un fichier firebot_control.launch pour lancer independamment
* test avec Rviz ==> compil mais problèmes avec Gazebo (pas de robot_description dans le server de parametre ? rviz ==> oui)

Correction du bug a cause duquel le parametre /robot_description n'était pas présent dans le serveur de parametre (correction de la config)

Test 2 :

* Tentative avec rostopic de publier un msg pour faire bouger les roues ==> echec
* Recherche sur diff_drive_controller ==> creation d'un nouveau package firebotv2 pour tester

**Semaine du 7 au 11 octobre 2019**

**mercredi 9 octobre 2019**

Creation d'un package firebotv2 qui s'appuis sur mes tests précédent (firebot), il est plus propre.

* Suivis de l'exemple de Epuck (fichier sur le depot git)
* Les Modeles gazebo et Rviz sont charger et sans bugs (pour le moment)
* faire quelques modif sur le modele du robot + implementer la commande

**semaine du 14 au 18 octobre 2019**

**Dimanche 20 octobre**
Modifiaction du model URDF ==> firebotv3 ajout d'une camera + plugin_camera dans gazebo + visualisation du rendu avec Rviz

* Exemple sur le site de ROS/tutorials http://wiki.ros.org/ROS/Tutorials
* TODO :  Mettre à jour le .yaml pour les valeur de pid, voir implementation de ODOMETRY

**semaine du 21 au 25**

**Lundi 21 jusqu'au Mercredi 23**

Travail sur le Firebot et la commande differentiel ==> Differential Drive robot.
Étude du tutorial ==> http://wiki.ros.org/diff_drive_controller

**Samedi 16 novembre 2019**
Création du PowerPoint.
 


