#include <ros/ros.h>
#include <geometry_msgs/Twist.h>

//---- Include le msg : motion----//
#include <firebotv2/motion.h>

//---- Constante pour le Firebot ----/
const float vel_avant = 2.0;
const float vel_gauche = 2.0;
const float vel_droite = -2.0;
const float vel_surplace = -5.0;

class Firebotv2
{
public:
    //---- Variable pour le mouvement à faire ----//
    int msg;
    //---- Creation de la methode pour bouger le Firebot ----//
    void firebotv2Move(geometry_msgs::Twist velocity, ros::Publisher pub_vitesse)
    {
        if (msg == 1)
        {
            velocity.linear.x = vel_avant;
            velocity.angular.z = 0.0;      //angular.z = 0.0 car on ne tourne pas
            pub_vitesse.publish(velocity); //publie la vitesse directement au bon topic
            ROS_INFO("en avant !!");
        }
        if (msg == 2)
        {
            velocity.linear.x = 0.1;
            velocity.angular.z = vel_droite;
            pub_vitesse.publish(velocity);
            ROS_INFO("tourne à droite");
        }
        if (msg == 3)
        {
            velocity.linear.x = 0.1;
            velocity.angular.z = vel_gauche;
            pub_vitesse.publish(velocity);
            ROS_INFO("tourne à gauche");
        }
        if (msg == 4)
        {
            velocity.linear.x = 0.0; //linear.x = 0.0 car on n'avance pas
            velocity.angular.z = vel_surplace;
            pub_vitesse.publish(velocity);
            ROS_INFO("Sur-place");
        }
    }

    //---- Callback pour le message ----//
    void motionMessageClbk(const firebotv2::motion::ConstPtr &motion)
    {
        msg = motion->motion;
        ROS_INFO("Motion CALLBACK");
    }
};

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Firebotv2_move");
    ros::NodeHandle nfirebot;
    Firebotv2 monFirebot;
    ros::Subscriber sub_firebotv2 = nfirebot.subscribe("/motion", 10, &Firebotv2::motionMessageClbk, &monFirebot); // creation du subscriber
    geometry_msgs::Twist vitesse;
    ros::Publisher pub_firebotv2 = nfirebot.advertise<geometry_msgs::Twist>("/firebotv2/firebotv2_controller/cmd_vel", 10); //creation du publisher
    ros::Rate loop_rate(10);
    ROS_INFO("MOUVEMENT DU FIRE BOT");
    while (ros::ok())
    {
        ros::spinOnce();
        monFirebot.firebotv2Move(vitesse, pub_firebotv2); //publication de la commande
        loop_rate.sleep();
    }
    return 0;
}
